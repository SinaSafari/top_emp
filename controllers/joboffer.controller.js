const { Applicants } = require('../models/Applicants')
const { JobOffer } = require('../models/JobOffer')
const { createJobOffer, getAllJobOffers, getSingleJobOffer, updateJobOffer } = require('../repositories/joboffers.repositories')

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllJobOffersController = async (req, res) => {
  return res.json({ job_offers: await getAllJobOffers({ dept_id: req.query.dept_id || "" }) })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getSingleJobOfferController = async (req, res) => {
  return res.json({ job_offer: await getSingleJobOffer(req.params.id) })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createJobOfferController = async (req, res) => {
  // await createJobOffer({ dept_id: req.body.dept_id, title: req.body.title, content: req.body.content, created_at: Date.now() })
  await JobOffer.query().insertGraph({ department: { id: req.body.department }, title: req.body.title, content: req.body.content, created_at: Date.now() }, { relate: true })
  return res.status(201).json({ msg: "done" })
}


/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getApplicantsOfAJobOffer = async (req, res) => {

  const data = await Applicants.query().withGraphFetched('jobOffer').where('job_offer_id', req.params.id)
  return res.json({ data })
}