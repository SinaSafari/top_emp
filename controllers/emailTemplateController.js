const { createEmailTemplate, updateTemplate, getAllEmailTemplates, getSingleEmailTemplates, deleteTemplate } = require('../repositories/EmailTemplates.repositories')



/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllEmailTemplateController = async (req, res) => {
  return res.json({ data: await getAllEmailTemplates() })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getSingleEmailTemplateController = async (req, res) => {

}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createEmailTemplateController = async (req, res) => {

}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.updateEmailTemplateController = async (req, res) => {

}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.deleteEmailTemplateController = async (req, res) => {

}