const { Role } = require('../models/Role')
const { createRole, getAllRoles } = require('../repositories/roles.repository')

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createRoleController = async (req, res) => {
  return res.json({ data: await createRole(req.body) })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllRolesController = async (req, res) => {
  return res.json({ data: await getAllRoles() })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.deleteRoleController = async (req, res) => {
  await Role.query().where("id", req.params.id).delete()
  return res.json({})
}