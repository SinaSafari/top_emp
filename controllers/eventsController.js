const { EventModel } = require('../models/Event')
const { getAllEvents, getEventsOfAUser, updateEvent, createEvent } = require('../repositories/events.repository')

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllEventsController = async (req, res) => {
  // return res.json({ data: await getAllEvents() })
  const data = [
    {
      id: 1,
      title: 'available times for interview.',
      start: new Date(2021, 10, 23, 8, 0, 0),
      end: new Date(2021, 10, 23, 10, 30, 0),
      type: "available"
    },
    {
      id: 2,
      title: 'available times for interview.',
      start: new Date(2021, 10, 24, 13, 0, 0),
      end: new Date(2021, 10, 24, 14, 30, 0),
      type: "taken"
    },
  ]
  return res.json({ data: await getAllEvents() })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getEventsOfAUserController = async (req, res) => {
  return res.json({ data: await getEventsOfAUser(req.params.id) })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.updateEventController = async (req, res) => {
  return res.json({ data: await updateEvent(req.params.id, req.body) });
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createEventController = async (req, res) => {
  await createEvent(req.body)
  const events = await EventModel.query()
  return res.status(201).json({ msg: "events created", events: events })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.deleteEventController = async (req, res) => {
  await EventModel.query().del().where("id", req.params.id)
  return res.status(201).json({ msg: "done" })
}