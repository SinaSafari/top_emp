const { reserveTimeByApplicant, createAvalableTime, getAllAvailableTimes } = require('../repositories/times.repositories')

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.reserveTimeByApplicantController = async (req, res) => {
  await reserveTimeByApplicant(req.params.id, req.body.id)
  return res.status(201).json({})
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createAvailableTime = async (req, res) => {
  // todo: validation
  await createAvalableTime(req.body)
  return res.status(201).json({})
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllAvailableTimes = async (req, res) => {

  return res.status(200).json({ data: await getAllAvailableTimes() })
}