const { singWithPayload } = require('../services/auth/jwt')
const { verifyPswd } = require('../services/auth/pwdEncryption')
const { getUser } = require('../repositories/user.repository')


/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.login = async (req, res) => {
  const { email, password } = req.body
  const user = await getUser(email)

  if (!user) {
    return res.status(404).json({ message: "user not found" })
  }

  if (!(await verifyPswd(password, user.password))) {
    return res.status(401).json({ message: "password wrong" })
  }

  delete user.password

  const token = singWithPayload({ email: user.email, role: user.role })

  res.cookie('token', { token, user })

  return res.status(200).json({
    token: token,
    user
  })
}