const { Applicants } = require('../models/Applicants');
const { getAvailableTimesBasedOnApplicant, updateAppicant, createApplicantRepository } = require('../repositories/applicants.repositories')
const formidable = require('formidable')
const { storeFile } = require('../lib/utils/upload');
const { JobOffer } = require('../models/JobOffer');

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAvailableTimesBasedOnApplicantController = async (req, res) => {
  return await getAvailableTimesBasedOnApplicant(req.params.id)
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.updateApplicantsController = async (req, res) => {
  console.log(" id: ", req.params.id, " body: ", req.body)
  return res.status(202).json({ data: await updateAppicant(req.params.id, req.body) });
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getSingleApplicant = async (req, res) => {
  return res.json({ data: await Applicants.query().where('id', req.params.id).first() })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createApplicantController = async (req, res) => {

  const { err, fields, files } = await new Promise((resolve, reject) => {
    const form = formidable();

    form.parse(req, (err, fields, files) => {
      if (err) reject({ err });
      resolve({ err, fields, files });
    });
  });

  if (err) {
    console.log(err)
    return res.status(400).json({})
  }

  console.log("files: ",files);
  

  const { absolutePath, pathInUrl } = await storeFile(files.resume)

  const job = await JobOffer.query().where("title", fields.job_offer).orWhere("id", fields.job_offer).first()

  await createApplicantRepository({
    email: fields.email,
    phone: fields.phone,
    description: fields.description,
    job_offer_id: job.id,
    status: req.params.sourceofreq === "dash" ? "accepted_by_hr" : "in_progress",
    source: fields.source,
    resume_path: pathInUrl,
    fullname: fields.fullname ?? "unknown"
  })

  return res.status(200).json({ msg: "done" })
}