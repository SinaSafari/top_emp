const { Status } = require('../models/Status')

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllStatusesController = async (req, res) => {
  return res.json({ data: await Status.query() })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createStatusController = async (req, res) => {
  await Status.query().insert(req.body)
  return res.json({})
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.deleteStatusController = async (req, res) => {
  await Status.query().where("id", req.params.id).delete()
  return res.json({})
}