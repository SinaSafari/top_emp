const {
  getAllDepartments,
  updateDepartment,
  getSingleDepartment,
  deleteDepartment,
  createDepartment,
} = require('../repositories/departments.repositories')

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.departmentsListController = async (req, res) => {
  return res.status(200).json({ departments: await getAllDepartments() })
}


/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getSingleDepartmentController = async (req, res) => {
  return res.status(200).json({ data: await getSingleDepartment(req.params.id) })
}


/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createDepartmentController = async (req, res) => {
  return res.status(200).json({ data: await createDepartment(req.body) })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.updateDepartmentController = async (req, res) => {
  return res.status(202).json({ data: await updateDepartment(req.params.id, req.body) })
}

/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.deleteDepartmentController = async (req, res) => {
  await deleteDepartment(req.params.id)
  return res.status(204).json({ data: "" })
}