const { User } = require('../models/User')
const { createUser, getUser, getAllUsers } = require('../repositories/user.repository')
const { encryptPswd } = require('../services/auth/pwdEncryption')
/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.createUserController = async (req, res) => {
  const {
    email,
    fullname,
    username,
    password,
    selectedDepartment,
    selectedRole,
    role,
  } = req.body

  const encryptedPassword = await encryptPswd(password)

  await User.query().insertGraph({
    email,
    fullname,
    username,
    password: encryptedPassword,
    role_type: selectedRole,
    dept_id: selectedDepartment,
    role_id: role,
  })
  return res.json({})
}


/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllUsersController = async (req, res) => {
  return res.json({ data: await getAllUsers() })
}


/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 */
exports.getAllSingleController = async (req, res) => {
  return res.json({ data: await User.query().where("id", req.params.id).first() })
}
