const { AvailableTime } = require('../models/AvailableTime')


exports.getAllAvailableTimes = async () => {
  return await AvailableTime.query().withGraphFetched('user').withGraphFetched('applicant')
}

exports.reserveTimeByApplicant = async (applicant_id, time_id) => {
  try {
    // todo error handling
    await AvailableTime.query().where('id', time_id).update({ applicant_id: applicant_id })
    return true
  } catch (err) {
    return false
  }
}

exports.createAvalableTime = async (data) => {
  try {
    // todo erorr handling
    await AvailableTime.query().insert(data)
    return true
  } catch (err) {
    return false
  }
}