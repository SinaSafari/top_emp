const { Role } = require('../models/Role')

exports.getAllRoles = async () => {
  return await Role.query()
}

exports.createRole = async (data) => {
  return await Role.query().insert(data)
}