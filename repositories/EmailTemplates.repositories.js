const { EmailTemplates } = require('../models/EmailTemplates')

exports.getAllEmailTemplates = async () => {
  return await EmailTemplates.query()
}

exports.getSingleEmailTemplates = async (id) => {
  return await EmailTemplates.query().where("id", id)
}

exports.createEmailTemplate = async (content) => {
  return await EmailTemplates.query().insert(content)
}

exports.updateTemplate = async (id, content) => {
  return await EmailTemplates.query().where("id", id).update(content)
}

exports.deleteTemplate = async (id) => {
  return await EmailTemplates.query().delete().where("id", id)
}

