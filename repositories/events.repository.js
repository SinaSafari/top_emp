const { EventModel } = require('../models/Event')

exports.getAllEvents = async () => {
  return await EventModel.query()
}

exports.getEventsOfAUser = async (user_id) => {
  return await EventModel.query().where("user_id", user_id)
}

exports.updateEvent = async (id, data) => {
  return await EventModel.query().where("id", id).update(data)
}

exports.createEvent = async (data) => {
  return await EventModel.query().insert(data)
}