const { User } = require('../models/User')

exports.getUser = async (email) => {
  return await User.query().where("email", email).first()
}

exports.createUserController = async (data) => {
  return await User.query().insert(data)
}

exports.getAllUsers = async () => {
  return await User.query()
}