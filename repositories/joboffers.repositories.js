const { Department } = require('../models/Department')
const { JobOffer } = require('../models/JobOffer')

exports.getAllJobOffers = async ({ dept_id }) => {
  if (dept_id) {
    const dept = await Department.query().where("title", dept_id).first()
    if (!dept) {
      return []
    }
    return await JobOffer.query().withGraphFetched('department').withGraphFetched("applicants").where("dept_id", dept.id)
  }
  return await JobOffer.query().withGraphFetched('department').withGraphFetched("applicants")
}

exports.getSingleJobOffer = async (id) => {
  return await JobOffer.query().findById(id).withGraphFetched('department')
}

exports.createJobOffer = async (data) => {
  return await JobOffer.query().insert(data)
}

exports.updateJobOffer = async (id, data) => {
  return await JobOffer.query().where("id", id).update(data)
}

exports.deleteJobOffer = async (id) => {
  return await JobOffer.query().delete()
}

exports.getApplicantsOfAJobOffer = async (job_offer_id) => {
  return await JobOffer.query().where("id", job_offer_id).withGraphFetched("applicants")
}