const { Applicants } = require('../models/Applicants')


exports.getAllApplicants = async () => {
  // TODO: pagination
  return await Applicants.query();
}


exports.getSingleApplicants = async (id) => {
  return await Applicants.query().where("id", id).first()
}

exports.getAvailableTimesBasedOnApplicant = async (id) => {
  return await Applicants.query().where("id", id).first().withGraphFetched({ jobOffer: { department: { availableTimes: true } } })
}

exports.updateAppicant = async (id, data) => {
  return await Applicants.query().where("id", id).update(data);
}


exports.createApplicantRepository = async (data) => {
  console.log(data)
  return await Applicants.query().insert(data)
}