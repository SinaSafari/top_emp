const { Department } = require("../models/Department")


exports.getAllDepartments = async () => {
  return await Department.query()
}

exports.getSingleDepartment = async (id) => {
  return await Department.query().where("id", id)
}

exports.createDepartment = async (data) => {
  return await Department.query().insert(data)
}

exports.updateDepartment = async (id, data) => {
  return await Department.query().where("id", id).update(data)
}

exports.deleteDepartment = async () => {
  return await Department.query().where("id", id).delete()
}