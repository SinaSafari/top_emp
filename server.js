require('dotenv').config()

const express = require('express')

const { runApp, setMiddlewares } = require('./config/server.config')
const { setRouters } = require('./routes/index.routes')
const { pipe } = require('./lib/utils/pipe')

const { initialDb } = require('./database/intialize')
const Knex = require('knex')
const { Model } = require('objection')
const { development } = require('./knexfile')


const main = async () => {
  const app = express()
  const knex = Knex(development)
  Model.knex(knex)

  pipe(
    setMiddlewares,
    // initialDb,
    setRouters,
    runApp
  )(app)

}

main()