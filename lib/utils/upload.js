const { createHash } = require("crypto")
const { extname } = require("path")
const { rename } = require("fs/promises")


const publicPath = "resumes"

async function generateFileName(fileName) {
  if (typeof fileName !== "string") return null;
  const now = Date.now();
  return createHash("SHA256").update(`${fileName}${now}`).digest("hex");
}

async function storeFile(file, directory = publicPath) {
  const filePath = `${process.cwd()}/public/${directory}`;
  const filename = await generateFileName(file.originalFilename);
  const newFile = `${filePath}/${filename}${extname(file.originalFilename)}`;
  const err = await rename(file.filepath, newFile);
  if (err) return null;
  return {
    absolutePath: newFile,
    pathInUrl: `/resumes/${filename}${extname(file.originalFilename)}`,
  };
}

module.exports = {
  storeFile,
  generateFileName,
}