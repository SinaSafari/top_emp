const fecha = require('fecha')


exports.generateDateTime = ({
  year = new Date().getFullYear(),
  month = new Date().getMonth(),
  day = new Date().getDay(),
  hour = new Date().getHours(),
  minutes = 0
}) => {
  return fecha.format(new Date(year, month, day, hour, minutes), "YYYY-MM-DD HH:mm:ss")
}
