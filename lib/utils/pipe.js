const pipe = (...functions) => x => functions.reduce((acc, fn) => fn(acc), x);


module.exports = { pipe }