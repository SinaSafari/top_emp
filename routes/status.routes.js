const { createStatusController, getAllStatusesController, deleteStatusController } = require('../controllers/statusController')


const express = require('express');
const statusController = express.Router()

statusController.route("/").get(getAllStatusesController).post(createStatusController)
statusController.route("/:id").delete(deleteStatusController)

module.exports = { statusController }