const swaggerUi = require('swagger-ui-express')
const { baseSwaggerConfig } = require('../docs/openapi/index.swagger')

const { departmentsRouter } = require('./departments.routes')
const { joboffersRouter } = require('./joboffers.routes')
const { authRouter } = require('./auth.routes')
const { usersRouter } = require('./users.routes')
const { timesRouter } = require('./times.routes')
const { applicantsRouter } = require('./applicants.routes')
const { eventsRouter } = require('./events.routes')
const { roleRouter } = require("./roles.routes")
const { statusController } = require("./status.routes")
const { emailTemplatesRouter } = require('./emailTemplates.routes')

/**
 * 
 * @param {import('express').Application} app 
 * @returns {import('express').Application}
 */
const setRouters = (app) => {
  app.use("/api/departments", departmentsRouter)
  app.use("/api/joboffers", joboffersRouter)
  app.use("/api/auth", authRouter)
  app.use("/api/users", usersRouter)
  app.use("/api/times", timesRouter)
  app.use("/api/applicants", applicantsRouter)
  app.use("/api/events", eventsRouter)
  app.use("/api/roles", roleRouter)
  app.use("/api/statuses", statusController)
  app.use("/api/emailtemplates", emailTemplatesRouter)

  // swagger router
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(baseSwaggerConfig))

  return app
}


module.exports = { setRouters }