const express = require('express');
const { User } = require('../models/User');
const { isAuthenticated, authorize } = require('../middlewares/auth.middleware')
const { createUserController, getAllUsersController, getAllSingleController } = require('../controllers/users.controller')
const usersRouter = express.Router()


// usersRouter.get("/", async (req, res) => {
//   return res.json({ data: await User.query().withGraphFetched('events') })
// });


usersRouter.route("/").get(getAllUsersController).post(createUserController)
usersRouter.route("/:id").get(getAllSingleController)

module.exports = { usersRouter }