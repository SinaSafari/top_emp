const { createEmailTemplateController, deleteEmailTemplateController, getAllEmailTemplateController, getSingleEmailTemplateController, updateEmailTemplateController } = require('../controllers/emailTemplateController')


const express = require('express');
const emailTemplatesRouter = express.Router()

emailTemplatesRouter.route("/").get(getAllEmailTemplateController).post(createEmailTemplateController)
emailTemplatesRouter.route("/:id").delete(getSingleEmailTemplateController).put(updateEmailTemplateController).delete(deleteEmailTemplateController)

module.exports = { emailTemplatesRouter }