const express = require('express')
const {
  getAllEventsController,
  updateEventController,
  getEventsOfAUserController,
  createEventController,
  deleteEventController
} = require('../controllers/eventsController')

const eventsRouter = express.Router()


eventsRouter
  .route("/")
  .get(getAllEventsController)
  .post(createEventController)

eventsRouter
  .route("/:id")
  .get(getEventsOfAUserController)
  .put(updateEventController)
  .delete(deleteEventController)


module.exports = { eventsRouter }