const express = require('express');
const { Applicants } = require('../models/Applicants');
const { AvailableTime } = require('../models/AvailableTime');
const { getAvailableTimesBasedOnApplicantController } = require('../controllers/applicantsController')
const { createAvailableTime, reserveTimeByApplicantController, getAllAvailableTimes } = require('../controllers/timesControllers')
const timesRouter = express.Router()


timesRouter
  .route("/")
  .get(getAllAvailableTimes)
  .post(createAvailableTime)



timesRouter
  .route("/interview/:id")
  .get(getAvailableTimesBasedOnApplicantController)
  .post(reserveTimeByApplicantController)



module.exports = { timesRouter }