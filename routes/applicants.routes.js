const express = require('express')
const { getAvailableTimesBasedOnApplicantController, updateApplicantsController, getSingleApplicant, createApplicantController } = require('../controllers/applicantsController')

const applicantsRouter = express.Router()

applicantsRouter.get("/available_times/:id", getAvailableTimesBasedOnApplicantController)
applicantsRouter.put("/:id", updateApplicantsController)
applicantsRouter.post("/", createApplicantController)
applicantsRouter.get("/:id", getSingleApplicant)

module.exports = { applicantsRouter }