const express = require('express');
const { createRoleController, getAllRolesController, deleteRoleController } = require('../controllers/rolesController')
const roleRouter = express.Router()



roleRouter.route("/").get(getAllRolesController).post(createRoleController)
roleRouter.route("/:id").delete(deleteRoleController)


module.exports = { roleRouter }