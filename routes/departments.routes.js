const express = require('express')
const {
  departmentsListController,
  createDepartmentController,
  deleteDepartmentController,
  getSingleDepartmentController,
  updateDepartmentController,
} = require('../controllers/departments.controller')

const departmentsRouter = express.Router()

departmentsRouter
  .route("/")
  .get(departmentsListController)
  .post(createDepartmentController)

departmentsRouter
  .route("/:id")
  .get(getSingleDepartmentController)
  .put(updateDepartmentController)
  .delete(deleteDepartmentController)

module.exports = { departmentsRouter }