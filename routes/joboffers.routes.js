const express = require('express')
const { getAllJobOffersController, getSingleJobOfferController, getApplicantsOfAJobOffer, createJobOfferController } = require('../controllers/joboffer.controller')
const joboffersRouter = express.Router()


joboffersRouter.get("/", getAllJobOffersController);
joboffersRouter.post("/", createJobOfferController)
joboffersRouter.route('/:id').get(getSingleJobOfferController)
joboffersRouter.get('/:id/applicants', getApplicantsOfAJobOffer)


module.exports = { joboffersRouter }