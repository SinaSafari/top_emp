const { Model } = require('objection')

class EventModel extends Model {
  static get tableName() {
    return "events"
  }

  static get relationMappings() {
    const { User } = require('./User')
    const { Applicants } = require('./Applicants')

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "events.user_id",
          to: "",
        }
      },
      applicant: {
        relation: Model.BelongsToOneRelation,
        modelClass: Applicants,
        join: {
          from: "events.applicant_id",
          to: "applicants.id",
        }
      }
    }
  }
}

module.exports = { EventModel }