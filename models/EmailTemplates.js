const { Model } = require('objection')


class EmailTemplates extends Model {
  static get tableName() {
    return "email_templates"
  }
}

module.exports = { EmailTemplates }