const { Model } = require('objection')

class JobOffer extends Model {
  static get tableName() {
    return "job_offers"
  }

  static get relationMappings() {
    const { Department } = require('./Department')
    const { Applicants } = require('./Applicants')
    return {
      department: {
        relation: Model.BelongsToOneRelation,
        modelClass: Department,
        join: {
          from: "job_offers.dept_id",
          to: "departments.id",
        }
      },
      applicants: {
        relation: Model.HasManyRelation,
        modelClass: Applicants,
        join: {
          from: "job_offers.id",
          to: "applicants.job_offer_id",
        }
      }
    }
  }
}

module.exports = { JobOffer }