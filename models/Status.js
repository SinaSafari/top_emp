const { Model } = require('objection')

class Status extends Model {
  static get tableName() {
    return "statuses"
  }
}

module.exports = { Status }