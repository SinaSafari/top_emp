const { Model } = require('objection')


class Applicants extends Model {
  static get tableName() {
    return "applicants"
  }

  static get relationMappings() {
    const { JobOffer } = require('./JobOffer')
    const { EventModel } = require('./Event')
    return {
      jobOffer: {
        relation: Model.BelongsToOneRelation,
        modelClass: JobOffer,
        join: {
          from: "applicants.job_offer_id",
          to: "job_offers.id"
        }
      },
      event: {
        relation: Model.HasManyRelation,
        modelClass: EventModel,
        join: {
          from: "applicants.id",
          to: "events.applicant_id",
        }
      }
    }
  }

}

module.exports = { Applicants }