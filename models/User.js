const { Model } = require('objection')


class User extends Model {
  static get tableName() {
    return "users"
  }

  static get relationMappings() {
    const { EventModel } = require('./Event')
    const { Role } = require('./Role')

    return {
      events: {
        relation: Model.HasManyRelation,
        modelClass: EventModel,
        join: {
          from: "users.id",
          to: "events.user_id",
        }
      },
      role: {
        relation: Model.BelongsToOneRelation,
        modelClass: Role,
        join: {
          from: "users.role_id",
          to: "roles.id",
        }
      }
    }
  }
}

module.exports = { User }