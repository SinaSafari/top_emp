const { Model } = require('objection')


class AvailableTime extends Model {
  static get tableName() {
    return "available_times"
  }

  static get relationMappings() {
    const { User } = require('./User')
    const { Applicants } = require('./Applicants')
    const { Department } = require('./Department')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "available_times.user_id",
          to: "users.id"
        }
      },
      deaprtment: {
        relation: Model.BelongsToOneRelation,
        modelClass: Department,
        join: {
          from: "available_times.dept_id",
          to: "departments.id"
        }
      },
      applicant: {
        relation: Model.BelongsToOneRelation,
        modelClass: Applicants,
        join: {
          from: "available_times.applicant_id",
          to: "applicants.id"
        }
      }
    }
  }

}

module.exports = { AvailableTime }