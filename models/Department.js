const { Model } = require('objection')

const DEPT = Object.freeze({
  TECH: "TECH",
  MARKETING: "MARKETING",
  HR: "HR",
  PRODUCT: "PRODUCT"
})


class Department extends Model {
  static get tableName() {
    return "departments"
  }

  static get relationMappings() {
    const { User } = require('./User')
    const { AvailableTime } = require('./AvailableTime')

    return {
      availableTimes: {
        relation: Model.HasManyRelation,
        modelClass: AvailableTime,
        join: {
          from: "departments.id",
          to: "available_times.dept_id"
        }
      }
    }
  }
}

module.exports = { Department }