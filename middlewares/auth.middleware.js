const { checkForToken } = require('../services/auth.service')
const { User } = require('../models/User')
const { verify } = require('jsonwebtoken')


/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @param {import('express').NextFunction} next 
 */
exports.isAuthenticated = async (req, res, next) => {
  try {
    const token = checkForToken(req)
    console.log("token", token)

    if (!token) {
      return res.status(403).json({})
    }

    const decoded = verify(token, "secret")
    console.log("decoded", decoded)

    const user = await User.query().where("email", decoded.email).first()
    req.user = user

    next()

  } catch (err) {
    console.log(err)
    res.status(403).json({})
  }
}


/**
 * 
 * @param  {...string} roles 
 * @returns 
 */
exports.authorize = (...roles) => {
  /**
   * 
   * @param {import('express').Request} req 
   * @param {import('express').Response} res 
   * @param {import('express').NextFunction} next 
   */
  return (req, res, next) => {
    console.log("authorize: ", req.user)
    if (!roles.includes(req.user.role)) {
      return res.status(403).json({ message: "not allowed" })
    }
    next()
  }
}