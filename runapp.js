const { exec } = require('child_process')


const main = () => {

  const serverRunner = exec('node server.js')
  const clientRunner = exec("cd client && npm run dev")

  serverRunner.on('error', (err) => {
    console.log(err.toString())
    process.exit()
  })

  clientRunner.on('error', (stream) => {
    console.log(stream)
    process.exit()
  })
}

main()
