import { LOGIN_FAILIURE, LOGIN_REQUEST, LOGIN_SUCCESS } from './constants'
import { ServiceContainer } from '../services/serviceContainer'

// login actions
export const loginRequest = async (dispatch, data, router) => {
  try {
    dispatch({ type: LOGIN_REQUEST })
    const res = await ServiceContainer.http.loginRequset(data)
    if (!res.success) {
      dispatch({
        type: LOGIN_FAILIURE,
        payload: res.message
      })
    }

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data,
    })

    ServiceContainer.storage.setToken(res.data.token)

    router.push('/dashboard')

  } catch (err) {
    dispatch({
      type: LOGIN_FAILIURE,
      payload: err.toString(),
    })
  }
}