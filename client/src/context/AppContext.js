import { createContext, useContext, useReducer } from "react";
import { loginReducer } from './reducers'
const AppStateContext = createContext()
const AppDispatchContext = createContext()


export const AppContextProvider = ({ children }) => {

  const [loginState, loginDispatch] = useReducer(loginReducer, { loading: false, loginData: {}, errorMessage: "" })

  return (
    <AppDispatchContext.Provider value={{ loginDispatch }}>
      <AppStateContext.Provider value={{ loginState }}>
        {children}
      </AppStateContext.Provider>
    </AppDispatchContext.Provider>
  )
}

export const useCtxState = () => useContext(AppStateContext)
export const useCtxDispatch = () => useContext(AppDispatchContext)