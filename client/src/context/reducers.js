import { LOGIN_FAILIURE, LOGIN_REQUEST, LOGIN_SUCCESS } from './constants'

export const loginReducer = (state, action) => {
  const { type, payload } = action

  switch (type) {
    case LOGIN_REQUEST:
      return { ...state, loading: true }
    case LOGIN_SUCCESS:
      return { ...state, loading: false, loginData: payload, errorMessage: "" }
    case LOGIN_FAILIURE:
      return { ...state, loading: false, errorMessage: payload, loginData: {} }
    default:
      return state
  }
}