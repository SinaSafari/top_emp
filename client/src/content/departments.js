export const DepartmentsList = [
  {
    id: 1,
    title: "tech",
    roles: ["superadmin", "tech_admin"],
  },
  {
    id: 2,
    title: "product",
    roles: ["superadmin", "product_admin"],
  },
  {
    id: 3,
    title: "human resource",
    roles: ["superadmin", "hr_admin"],
  },
  {
    id: 4,
    title: "marketing",
    roles: ["superadmin", "marketing_admin"],
  },
  {
    id: 5,
    title: "finance",
    roles: ["superadmin", "finance_admin"],
  },
]
