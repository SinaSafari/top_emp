import React, { useEffect, useState } from 'react'
import { Calendar, momentLocalizer, Views } from 'react-big-calendar'
import moment from 'moment'
import { useRouter } from 'next/router'
import {
  Box,
  Button,
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  Flex,
  Text,
  useToast,
  Spinner,
} from "@chakra-ui/react"
import 'moment-timezone'
import { englishToPersianNumbers } from '../../lib/numbers'

moment.tz.setDefault('Asia/Tehran')

/**
 * @type {import('next').NextPage}
 */
const EventsPage = ({ eventsList }) => {
  const localizer = momentLocalizer(moment)

  const [events, setEvents] = useState([])
  const [selectedEvent, setSelectedEvent] = useState()
  const [drawerLoading, setdrawerLoading] = useState(false)
  const [creationLoading, setCreationLoading] = useState(false)

  const { onOpen, onClose, isOpen } = useDisclosure()

  const btnRef = React.useRef()

  const toast = useToast()
  const router = useRouter()


  const prepareEventDataForState = () => {
    let convertedData = []
    for (let i of eventsList) {
      convertedData = [
        ...convertedData,
        {
          ...i,
          id: i.id,
          start: new Date(i.start_time),
          end: new Date(i.end_time),
          title: "",
        }
      ]
    }
    return convertedData
  }

  const onDeleteHandler = async () => {
    try {
      setdrawerLoading(true)
      await fetch(`http://localhost:3100/api/events/${selectedEvent.id}`, { method: "DELETE" });

      toast({
        title: "event canceled successfully",
        description: "the event is not visible to applicant anymore.",
        status: "success",
        duration: 3000,
        isClosable: true,
      })

      const filtered = events.filter(i => i.id !== selectedEvent.id)
      setEvents(filtered)

    } catch (err) {
      toast({
        title: "something went wrong",
        description: "your changes did not submitt successfully",
        status: "error",
        duration: 3000,
        isClosable: true,
      })
    } finally {
      setdrawerLoading(false)
      onClose()
    }
  }



  const handleSelect = async ({ start, end }) => {
    try {
      setCreationLoading(true)
      // TODO: request to server
      const res = await fetch(
        "http://localhost:3100/api/events",
        {
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            user_id: 1,
            start_time: start.toString(),
            end_time: end.toString(),
            type: "available",
          }),
          method: "POST",
        })

      setEvents([
        ...events,
        {
          start,
          end,
          title: "available times for interview.",
          type: "available"
        },
      ])

      toast({
        title: "event created successfully",
        description: "the event is now available for applicant to choose.",
        status: "success",
        duration: 3000,
        isClosable: true,
      })
    } catch (err) {
      console.log(err);
    } finally {
      setCreationLoading(false)
    }

  }

  useEffect(() => {
    setEvents(() => prepareEventDataForState(eventsList))
  }, [])

  return (
    <Box>
      {creationLoading && (
        <Box
          zIndex={99999}
          position="fixed"
          width="100%"
          height="100%"
          backgroundColor="rgba(255,255,255,0.8)"
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="#fb5b1f"
            size="xl"
          />
        </Box>
      )}
      <Calendar
        selectable
        localizer={localizer}
        events={events}
        defaultView={Views.WEEK}
        views={{ week: true }}
        scrollToTime={new Date(1970, 1, 1, 6)}
        defaultDate={new Date()}
        onSelectSlot={handleSelect}
        eventPropGetter={(e) => {
          console.log(e);
          const background = e.type === "available" ? "#3174ad" : "#fb5b1f"
          return { style: { background: background } }
        }}
        onSelectEvent={(e) => {
          onOpen()
          setSelectedEvent(e)
        }}
        components={{
          event: ({ event }) => (
            <Box>
              <Box>
                <small>event id: {event.id}</small>
              </Box>
              <Flex my="3" justifyContent="center" alignItems="center" w="100%" h="100%">
                {
                  event.type === "available"
                    ? <Text fontSize="lg" textAlign="center">event is available</Text>
                    : <Text fontSize="lg" textAlign="center">This event is taken by an applicant</Text>
                }
              </Flex>
            </Box>
          )
        }}
      />
      <Drawer
        isOpen={isOpen}
        placement="right"
        onClose={onClose}
        finalFocusRef={btnRef}
        size={"lg"}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Date Information</DrawerHeader>
          <DrawerBody>
            <Box my="5">
              <Text fontSize="xl" fontWeight="bold">Date and time in Gregorian calendar</Text>
              {selectedEvent && (
                <>
                  <Box border={"1px solid #eee"} borderRadius={"10px"} p={"4"} my="5">
                    <Text>start time: {new Date(selectedEvent.start).toLocaleString()}</Text>
                  </Box>
                  <Box border={"1px solid #eee"} borderRadius={"10px"} p={"4"} my="5">
                    <Text>end time: {new Date(selectedEvent.end).toLocaleString()}</Text>
                  </Box>
                </>
              )}
            </Box>
            <Box my="5">
              <Text fontSize="xl" fontWeight="bold">Date and time in Persian calendar</Text>
              {selectedEvent && (
                <>
                  <Box border={"1px solid #eee"} borderRadius={"10px"} p={"4"} my="5">
                    <Text>start time: {englishToPersianNumbers(new Date(selectedEvent.start).toLocaleString("fa")).replace("،", ", ")}</Text>
                  </Box>
                  <Box border={"1px solid #eee"} borderRadius={"10px"} p={"4"} my="5">
                    <Text>end time: {englishToPersianNumbers(new Date(selectedEvent.end).toLocaleString("fa")).replace("،", ", ")}</Text>
                  </Box>
                </>
              )}
            </Box>
            <Flex flexDir="row-reverse" alignItems="center">
              {drawerLoading ? (
                <>
                  <Spinner
                    thickness="4px"
                    speed="0.65s"
                    emptyColor="gray.200"
                    color="#fb5b1f"
                    size="xl"
                  />
                </>
              ) : (
                <>
                  <Button
                    mx={1}
                    backgroundColor={"red"}
                    color="white"
                    border={"1px solid red"}
                    _active={{}}
                    _hover={{ color: "black", background: "white" }}
                    onClick={onDeleteHandler}
                  >
                    Delete Date
                  </Button>
                  <Button
                    mx={1}
                    onClick={onClose}
                  >
                    Cancel
                  </Button>
                </>
              )}
            </Flex>
          </DrawerBody>
        </DrawerContent>
      </Drawer>

    </Box>
  )
}

/**
 * @type {import('next').GetServerSideProps} 
 */
export const getServerSideProps = async (ctx) => {
  const res = await fetch("http://localhost:3100/api/events")
  const json = await res.json()

  return {
    props: {
      eventsList: json.data
    }
  }
}

export default EventsPage
