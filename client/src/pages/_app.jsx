import { MainLayout } from '../components/layout/MainLayout'
import '../styles/globals.css'
import "bootstrap/dist/css/bootstrap.min.css"
import '@asseinfo/react-kanban/dist/styles.css'
import 'react-pdf/dist/esm/Page/AnnotationLayer.css';
import "react-big-calendar/lib/css/react-big-calendar.css"
import Head from 'next/head'

import { ChakraProvider } from "@chakra-ui/react"
import { AppContextProvider } from '../context/AppContext'
import { useStore } from '../store/useStore'
import { useEffect } from 'react';
import { SessionProvider } from "next-auth/react"
import Router from 'next/router'
import nProgress from 'nprogress';
import "nprogress/nprogress.css";




function MyApp({ Component, pageProps: { session, ...pageProps }, }) {

  Router.events.on("routeChangeStart", () => nProgress.start())
  Router.events.on("routeChangeError", () => nProgress.done())
  Router.events.on("routeChangeComplete", () => nProgress.done())


  const { user, token } = useStore(s => s)
  useEffect(() => {

    const loadInitialStoreProps = () => {
      const authData = localStorage.getItem("auth")
      console.log(authData);
      if (authData) {
        const parsedData = JSON.parse(authData)
        useStore.setState({ user: parsedData.user, token: parsedData.token })
      }
    }

    if (Object.keys(user).length <= 0 || !token) {
      loadInitialStoreProps()
    } else {
      console.log({ user, token });
    }
  }, [])
  return (
    <>
      <Head>
        <title>top recruiter</title>
        <link rel="shortcut icon" type="image/jpg" href="/logo-top-fa.svg" />
      </Head>
      <SessionProvider session={session}>
        <AppContextProvider>
          <ChakraProvider>
            <MainLayout>
              <Component {...pageProps} />
            </MainLayout>
          </ChakraProvider>
        </AppContextProvider>
      </SessionProvider>
    </>
  )
}

export default MyApp
