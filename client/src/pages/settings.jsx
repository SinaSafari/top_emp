import React, { useEffect, useState } from 'react'

import {
  Tabs,
  TabList,
  TabPanels,
  TabPanel,
  Tab,
  Text,
} from "@chakra-ui/react";
import { UsersSettings } from '../components/settings/users/usersSettings'
import { RolesSetting } from '../components/settings/roles/rolesSetting'
import { StatusesSetting } from '../components/settings/statuses/statusSetting';
import EmailSettings from '../components/settings/emails/emailsSettings';


const SettingsPage = () => {
  return (
    <>
      <Tabs variant="enclosed">
        <TabList>
          <Tab
            _selected={{
              color: "white",
              bg: "#fb5b1f"
            }}
            _focus={{
              outline: "none"
            }}
          >
            users
          </Tab>
          <Tab
            _selected={{
              color: "white",
              bg: "#fb5b1f"
            }}
            _focus={{
              outline: "none"
            }}
          >
            roles
          </Tab>
          <Tab
            _selected={{
              color: "white",
              bg: "#fb5b1f"
            }}
            _focus={{
              outline: "none"
            }}
          >
            statuses
          </Tab>
          <Tab
            _selected={{
              color: "white",
              bg: "#fb5b1f"
            }}
            _focus={{
              outline: "none"
            }}
          >
            emails
          </Tab>
          <Tab
            _selected={{
              color: "white",
              bg: "#fb5b1f"
            }}
            _focus={{
              outline: "none"
            }}
          >
            other
          </Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <UsersSettings />
          </TabPanel>
          <TabPanel>
            {/* <p>roles tab</p> */}
            <RolesSetting />
          </TabPanel>
          <TabPanel>
            <StatusesSetting />
            {/* <p>statuses tab</p> */}
          </TabPanel>
          <TabPanel>
            <EmailSettings />
            {/* <p>statuses tab</p> */}
          </TabPanel>
          <TabPanel>
            <Text>Not Implemented</Text>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </>
  )
}


export const getServerSideProps = async (ctx) => {
  return {
    props: {}
  }
}

export default SettingsPage