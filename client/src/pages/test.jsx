import React from 'react'
import { useState } from 'react'
import { Editor } from '../components/common/Editor'

const Test = () => {
  const [content, setContent] = useState('<p>Hello world</p>')
  return (
    <div>
      <Editor contentState={content} setContentState={setContent} />
    </div>
  )
}

export default Test
