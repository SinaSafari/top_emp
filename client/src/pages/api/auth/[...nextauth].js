import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"

export default NextAuth({
  credentials: {
    username: { label: "Username", type: "text", placeholder: "jsmith" },
    password: { label: "Password", type: "password" }
  },
  // Configure one or more authentication providers
  providers: [
    CredentialsProvider({
      name: "credentials",
      async authorize(credentials, req) {
        try {
          console.log("credentials: ", credentials)
          console.log("req: ", req)

          let res = await fetch(
            "http://localhost:3100/api/auth/login",
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                email: credentials.email,
                password: credentials.password,
              })
            }
          )
          res = await res.json()

          if (Object.entries(res.user).length > 0) {
            return res.user
          } else {
            return null
          }
        } catch (err) {
          const errorMessage = err.response.data.message
          throw new Error(errorMessage + '&email=' + credentials.email)
        }
      }
    }),
  ],
  callbacks: {
    jwt: ({ token, user }) => {
      // first time jwt callback is run, user object is available
      if (user) {
        token.userId = user.id;
      }

      return token;
    },
    session: ({ session, token }) => {
      if (token) {
        session.userId = token.userId;
      }

      return session;
    },
  }
  // pages: {
  //   error: "/login"
  // }
})