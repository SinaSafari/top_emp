import React from "react";
import { useStore } from '../../store/useStore'
import {
  SimpleGrid,
  Box,
  Text,
  Heading,
  Avatar,
  Center,
  Stack,
  Button,
  useColorModeValue,
  Flex,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { getSession } from 'next-auth/react'

const DashboardIndex = ({ times, user }) => {
  // const { user } = useStore(s => s)
  return (
    <Box>
      <Text mb="10" fontSize="6xl" fontWeight="bold">Welcome</Text>
      <SimpleGrid columns={2} spacing={8}>
        <Box w="100%" border="1px solid rgba(0,0,0,0.1)" p={3} borderRadius={10}>
          <Text fontSize="3xl" fontWeight="bold">Personal Info</Text>
          <PersoanlInfoComp user={user} />
        </Box>
        <Box w="100%" border="1px solid rgba(0,0,0,0.1)" p={3} borderRadius={10}>
          <Text fontSize="3xl" fontWeight="bold">Incoming event</Text>
          {times.data.length ? (
            <>
              {times.data.map(item => (
                <Flex key={item.id} alignItems="center" justifyContent={"space-between"} borderRadius={10} border={"1px solid rgba(0,0,0,0.05)"} p={"3"} mb="2">
                  <Text>start: {new Date(item.start_time).toLocaleString()}</Text>
                  <Text>end: {new Date(item.end_time).toLocaleString()}</Text>
                  <Text>{item.type}</Text>
                </Flex>
              ))}
              {/* {JSON.stringify(times.data)} */}
            </>
          ) : (
            <>
              <Box display="flex" justifyContent="center" alignItems="center" width={"100%"} height="100%">
                <Text>
                  You don&rsquo;t have any event upcoming days
                </Text>

              </Box>
            </>
          )}

        </Box>
      </SimpleGrid>
    </Box>
  )
}


const PersoanlInfoComp = ({ user }) => {
  const router = useRouter()
  return (
    <Center py={6}>
      {/* <pre>
        <code>

          {JSON.stringify(user.data, null, 4)}
        </code>
      </pre> */}
      <Box
        // maxW={'320px'}
        w={'full'}
        bg={useColorModeValue('white', 'gray.900')}
        boxShadow={'2xl'}
        rounded={'lg'}
        p={6}
        textAlign={'center'}
      >
        <Avatar
          size={'xl'}
          src={
            '/profile.png'
          }
          alt={'Avatar Alt'}
          mb={4}
          pos={'relative'}
          _after={{
            content: '""',
            w: 4,
            h: 4,
            bg: 'green.300',
            border: '2px solid white',
            rounded: 'full',
            pos: 'absolute',
            bottom: 0,
            right: 3,
          }}
        />
        <Heading fontSize={'2xl'} fontFamily={'body'}>
          {user.data.fullname || "Name did not specified"}
        </Heading>
        <Text fontWeight={600} color={'gray.500'} mb={4}>
          @{user.data.username}
        </Text>
        <Box borderBottom="1px solid rgba(0,0,0,0.1)">
          <Flex p="2">
            <Box>
              <Text fontWeight="bold">Email</Text>
            </Box>
            <Box flexGrow="1">
              <Text>{user.data.email}</Text>
            </Box>
          </Flex>
        </Box>
        <Stack mt={8} direction={'row'} spacing={4}>
          <Button
            flex={1}
            fontSize={'sm'}
            rounded={'full'}
            _focus={{
              bg: 'gray.200',
            }}
            onClick={e => router.push("/events")}
          >
            Events
          </Button>
          <Button
            flex={1}
            fontSize={'sm'}
            rounded={'full'}
            bg={'blue.400'}
            color={'white'}
            boxShadow={
              '0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
            }
            _hover={{
              bg: 'blue.500',
            }}
            _focus={{
              bg: 'blue.500',
            }}
            onClick={e => router.push("/profile")}
          >
            Edit Profile
          </Button>
        </Stack>
      </Box>
    </Center>
  )
}

/**
 * @type {import('next').GetServerSideProps}
 */
export const getServerSideProps = async (ctx) => {
  // get events and so on
  const session = await getSession({ req: ctx.req })

  let resp = await fetch(`http://localhost:3100/api/events/${session.userId}`)
  resp = await resp.json()

  let userResp = await fetch(`http://localhost:3100/api/users/${session.userId}`)
  userResp = await userResp.json()

  return {
    props: {
      times: resp,
      user: userResp
    }
  }
}

export default DashboardIndex