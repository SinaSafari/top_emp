import React from 'react'

const HomePage = () => {
  return (
    <div>

    </div>
  )
}

/**
 * 
 * @type {import('next').GetServerSideProps} 
 */
export const getServerSideProps = async (ctx) => {
  return {
    props: {},
    redirect: {
      permanent: false,
      destination: "/login"
    }
  }
}

export default HomePage
