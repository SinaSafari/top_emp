import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { Box, Flex, GridItem, Stack, Text } from '@chakra-ui/layout'
import { FormControl, FormHelperText, FormLabel } from '@chakra-ui/form-control'
import { Input } from '@chakra-ui/input'
import { Button } from '@chakra-ui/button'
import { useColorModeValue, chakra, Textarea, Icon, Alert, Select, AlertIcon } from '@chakra-ui/react'

const AddApplicant = ({ data }) => {
  const router = useRouter()

  // TODO: these should fethced from server
  const sourcesList = [{ id: 1, title: "jobinja" }, { id: 2, title: "irantalent" }, { id: 3, title: "quera" }]

  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [description, setDescription] = useState('')
  const [resume, setResume] = useState(null)
  // department
  const [department, setDepartment] = useState('tech')
  // job offers
  const [jobOfferList, setJobOfferList] = useState([])
  // selected job offer
  const [selectedJobOffer, setSelectedJobOffer] = useState('')
  const [source, setSource] = useState('')
  const [loading, setLoading] = useState(false)
  const [srvMsg, setSrvMsg] = useState('')


  useEffect(() => {
    const loadData = async () => {
      const data = await getJobOffers('tech')
      setJobOfferList(data)
    }
    loadData()
  }, [])

  const submitHandler = async (e) => {
    try {
      e.preventDefault()
      setLoading(true)

      const fd = new FormData()
      fd.append("email", email)
      fd.append("phone", phone)
      fd.append("description", description)
      fd.append("resume", resume)
      fd.append("department", department)
      fd.append("job_offer", selectedJobOffer)
      fd.append("source", source)

      const res = await fetch(
        'http://localhost:3100/api/applicants',
        {
          method: "POST",
          body: fd,
        }
      )
      if (res.status < 200 && res.status < 399) {
        setSrvMsg('something went wrong...')
        return
      }
      router.push(`/applicants/${department}`)
    } catch (err) {
      setSrvMsg('something went wrong...')
    } finally {
      setLoading(false)
    }
  }

  const getJobOffers = async (dept) => {
    const res = await fetch(`http://localhost:3100/api/joboffers?dept_id=${dept}`)
    const json = await res.json()
    const jobOffers_data = json.job_offers.map(i => ({ id: i.id, title: i.title }))
    return jobOffers_data
  }

  const onSelectdepartment = async (e) => {
    const jobOffers_data = await getJobOffers(e.target.value)
    setDepartment(e.target.value)
    setJobOfferList(jobOffers_data)
  }

  return (
    <Box bg={useColorModeValue("gray.50", "inherit")} p={10}>
      <Text fontSize="6xl" fontWeight="bold">Adding Applicant</Text>
      {/* <Text fontSize="xl">this</Text> */}
      {srvMsg && (
        <Box my="5">
          <Alert status="error">
            <AlertIcon />
            There was an error processing your request
          </Alert>
        </Box>
      )}
      <Box>
        <Flex justifyContent="center" alignItems="center" >
          <chakra.form
            w={"100%"}
            method="POST"
            shadow="base"
            rounded={[null, "md"]}
            overflow={{ sm: "hidden" }}
          >
            <Stack
              px={4}
              py={5}
              bg={useColorModeValue("white", "gray.700")}
              spacing={6}
              p={{ sm: 6 }}
            >
              <div>
                <Flex justifyContent="center" alignItems="center" my="3">
                  <FormControl mx="1">
                    <FormLabel
                      htmlFor="email_address"
                      fontSize="sm"
                      fontWeight="md"
                      color={useColorModeValue("gray.700", "gray.50")}
                    >
                      Email address
                    </FormLabel>
                    <Input
                      type="text"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      name="email_address"
                      id="email_address"
                      placeholder="enter email address"
                      autoComplete="email"
                      mt={1}
                      focusBorderColor="brand.400"
                      shadow="sm"
                      size="sm"
                      w="full"
                      rounded="md"
                    />
                  </FormControl>
                  <FormControl mx="1">
                    <FormLabel
                      htmlFor="phone_number"
                      fontSize="sm"
                      fontWeight="md"
                      color={useColorModeValue("gray.700", "gray.50")}
                    >
                      Phone number
                    </FormLabel>
                    <Input
                      type="text"
                      value={phone}
                      onChange={(e) => setPhone(e.target.value)}
                      name="phone_number"
                      id="phone_number"
                      placeholder="enter phone number"
                      autoComplete="email"
                      mt={1}
                      focusBorderColor="brand.400"
                      shadow="sm"
                      size="sm"
                      w="full"
                      rounded="md"
                    />
                  </FormControl>
                </Flex>
                <FormControl id="email" mt={1}>
                  <FormLabel
                    fontSize="sm"
                    fontWeight="md"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Description and notes
                  </FormLabel>
                  <Textarea
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    placeholder="if there&apos;s any notes about this resume, please insert them here."
                    mt={1}
                    rows={3}
                    shadow="sm"
                    focusBorderColor="brand.400"
                    fontSize={{ sm: "sm" }}
                  />
                </FormControl>
                <Flex justifyContent="center" alignItems="center" my="3">
                  <FormControl mx="1">
                    <FormLabel
                      htmlFor="email_address"
                      fontSize="sm"
                      fontWeight="md"
                      color={useColorModeValue("gray.700", "gray.50")}
                    >
                      departments
                    </FormLabel>
                    <Select onChange={onSelectdepartment} defaultChecked="tech">
                      {data.departments.map(i => (
                        <option key={i.id} value={i.title}>{i.title}</option>
                      ))}
                    </Select>
                  </FormControl>
                  <FormControl mx="1">
                    <FormLabel
                      htmlFor="phone_number"
                      fontSize="sm"
                      fontWeight="md"
                      color={useColorModeValue("gray.700", "gray.50")}
                    >
                      job offer
                    </FormLabel>
                    <Select onChange={e => setSelectedJobOffer(e.target.value)}>
                      {jobOfferList.map(i => (
                        <option key={i.id} value={i.title}>{i.title}</option>
                      ))}
                    </Select>
                  </FormControl>
                  <FormControl mx="1">
                    <FormLabel
                      htmlFor="phone_number"
                      fontSize="sm"
                      fontWeight="md"
                      color={useColorModeValue("gray.700", "gray.50")}
                    >
                      source
                    </FormLabel>
                    <Select onChange={e => setSource(e.target.value)}>
                      {sourcesList.map(i => (
                        <option key={i.id} value={i.title}>{i.title}</option>
                      ))}
                    </Select>
                  </FormControl>
                </Flex>
              </div>
              <FormControl id="email" mt={1} maxW={"600"}>
                {/* TODO: add drag and drop feature*/}
                <FormLabel
                  fontSize="sm"
                  fontWeight="md"
                  color={useColorModeValue("gray.700", "gray.50")}
                >
                  upload resume
                </FormLabel>
                <Input type="file" onChange={(e) => setResume(e.target.files[0])} />
              </FormControl>
            </Stack>
            <Box
              px={{ base: 4, sm: 6 }}
              py={3}
              bg={useColorModeValue("gray.50", "gray.900")}
              textAlign="right"
            >
              <Button
                type="submit"
                // colorScheme="brand"
                onClick={submitHandler}
                background="#fb5b1f"
                color="white"
                _focus={{ shadow: "" }}
                fontWeight="md"
              >
                Add
              </Button>
            </Box>
          </chakra.form>
        </Flex>
        {/* </SimpleGrid> */}
      </Box>
      <Box visibility={{ base: "hidden", sm: "visible" }} aria-hidden="true">
        <Box py={5}>
          <Box
            borderTop="solid 1px"
            borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
          ></Box>
        </Box>
      </Box>
    </Box>
  );
}

/**
 * 
 * @type {import('next').GetServerSideProps}
 */
export const getServerSideProps = async (ctx) => {
  const res = await fetch('http://localhost:3100/api/departments')
  const data = await res.json()

  return {
    props: {
      data: data
    }
  }
}

export default AddApplicant
