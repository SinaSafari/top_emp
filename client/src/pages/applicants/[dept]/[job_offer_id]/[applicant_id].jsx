import { Flex, Box, Text, GridItem, Grid, useColorModeValue, Button, Spinner, Select } from '@chakra-ui/react';
import { useEffect, useState, } from 'react';
import { pdfjs, Document, Page, Outline } from 'react-pdf';
import { DownloadIcon, ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons'
import { useRouter } from 'next/router';

const SingleApplicant = ({ data }) => {
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [selectedStatus, setSelectedStatus] = useState()
  const [loading, setLoading] = useState(false)
  const [srvMsg, setSvrMsg] = useState('')
  const [statusList, setStatusList] = useState([])





  useEffect(() => {
    const statusesList = {
      applied: "applied",
      in_progress: "in_progress",
      accepted_by_hr: "accepted_by_hr",
      accepted_by_admin: "accepted_by_admin",
      first_interview: "first_interview",
      second_interview: "second_interview",
      third_interview: "third_interview",
      final_approval: "final_approval",
      talent_pool: "talent_pool",
      rejected: "rejected",
    }
    const prepareStatusList = () => {
      let s = []
      Object.entries(statusesList).forEach(([key, val], idx) => {
        s = [
          ...s,
          {
            id: idx,
            title: val.split("_").map(i => i.charAt(0).toUpperCase() + i.slice(1)).join(" "),
            value: val
          }
        ]
      })
      return s
    }
    const statuses = prepareStatusList()
    setStatusList(statuses)

    if (data.status) {
      setSelectedStatus(data.status)
    }
  }, [])



  const router = useRouter()

  const updateStatus = async () => {
    try {
      setLoading(true)
      const res = await fetch(`http://localhost:3100/api/applicants/${router.query.applicant_id}`, {
        method: "PUT",
        body: JSON.stringify({ status: selectedStatus }),
        headers: {
          'Content-Type': 'application/json'
        },
      })
      if (!(res.status < 200 && res.status > 400)) {
        setSvrMsg("something went wrong...")
      }
      router.push(`/applicants/${router.query.dept}/${router.query.job_offer_id}`)
    } catch (err) {
      setSvrMsg("something went wrong...")
    } finally {
      setLoading(false)
    }
  }

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
    setPageNumber(1);
  }

  function changePage(offset) {
    setPageNumber(prevPageNumber => prevPageNumber + offset);
  }

  function previousPage() {
    changePage(-1);
  }

  function nextPage() {
    changePage(1);
  }
  function onItemClick({ pageNumber: itemPageNumber }) {
    setPageNumber(itemPageNumber);
  }

  const LoadingPdf = () => (
    <Flex h="70vh" justifyContent="center" alignItems="center">
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="blue.500"
        size="xl"
      />
    </Flex>
  )

  const ErrorPdf = () => (
    <Flex h="70vh" justifyContent="center" alignItems="center">
      <Text fontSize="xl" fontWeight="bold">faild to preview pdf. please download the file</Text>
    </Flex>
  )


  useEffect(() => {
    pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.min.js`;
  }, [])

  return (
    <>
      <Grid templateColumns="repeat(5, 1fr)" gap={4}>
        <GridItem colSpan={2} h="10" >
          <Box border="1px solid rgba(0,0,0,0.1)" borderRadius="10" p="3" bg="white">
            <Box mb="3">
              <Text color="#fb5b1f">
                Full name
              </Text>
              <Text fontWeight="bold" fontSize="lg">
                {data.fullname}
              </Text>
            </Box>
            <Box visibility={{ base: "hidden", sm: "visible" }} aria-hidden="true">
              <Box py={5}>
                <Box
                  borderTop="solid 1px"
                  borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
                ></Box>
              </Box>
            </Box>
            <Box mb="3">
              <Text color="#fb5b1f">
                Email
              </Text>
              <Text fontWeight="bold" fontSize="lg">
                {data.email ? data.email : "not specified"}
              </Text>
            </Box>
            <Box visibility={{ base: "hidden", sm: "visible" }} aria-hidden="true">
              <Box py={5}>
                <Box
                  borderTop="solid 1px"
                  borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
                ></Box>
              </Box>
            </Box>
            <Box mb="3">
              <Text color="#fb5b1f">Phone number</Text>
              <Text fontWeight="bold" fontSize="lg">
                {data.phone ? data.phone : "not specified"}
              </Text>
            </Box>
            <Box visibility={{ base: "hidden", sm: "visible" }} aria-hidden="true">
              <Box py={5}>
                <Box
                  borderTop="solid 1px"
                  borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
                ></Box>
              </Box>
            </Box>
            <Box mb="3">
              <Text color="#fb5b1f">Cover Letter</Text>
              <Text fontWeight="bold" fontSize="lg">
                {data.cover_letter ? data.cover_letter : "not specified"}
              </Text>
            </Box>
            <Box visibility={{ base: "hidden", sm: "visible" }} aria-hidden="true">
              <Box py={5}>
                <Box
                  borderTop="solid 1px"
                  borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
                ></Box>
              </Box>
            </Box>


            <Box mb="3">
              <Text color="#fb5b1f">Update applicant status</Text>
              <Flex alignItems="center" justifyContent="space-between">
                <Box flex="1" px="3">
                  <Select value={selectedStatus} onChange={(e) => setSelectedStatus(e.target.value)}>
                    {statusList.map((i, idx) => {
                      return (
                        <option key={idx} value={i.value}>{i.title}</option>
                      )
                    })}
                  </Select>
                </Box>
                <Button
                  bg="#fb5b1f"
                  color="white"
                  _hover={{
                    bg: "#ebedf0",
                    color: "#fb5b1f",
                    borderColor: "#fb5b1f"
                  }}
                  _active={{
                    bg: "#fb5b1f",
                    color: "#fff"
                  }}
                  onClick={updateStatus}
                >
                  {loading ? (
                    <>
                      ...
                    </>
                  ) : (
                    <>
                      update
                    </>
                  )}
                </Button>
              </Flex>
            </Box>


            <Box visibility={{ base: "hidden", sm: "visible" }} aria-hidden="true">
              <Box py={5}>
                <Box
                  borderTop="solid 1px"
                  borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
                ></Box>
              </Box>
            </Box>

            <Box>
              <a target="_blank" href={`http://localhost:3100${data.resume_path}`} rel="noopener noreferrer">
                <Button
                  bg="#fb5b1f"
                  color="white"
                  leftIcon={<DownloadIcon />}
                  variant="solid"
                  _hover={{
                    bg: "#ebedf0",
                    color: "#fb5b1f",
                    borderColor: "#fb5b1f"
                  }}
                  _active={{
                    bg: "#fb5b1f",
                    color: "#fff"
                  }}
                // onClick={e => e.preventDefault()}
                >
                  Download Resume
                </Button>
              </a>

            </Box>
          </Box>
        </GridItem>
        <GridItem colStart={3} colEnd={6}>
          <Flex flexDir="column" justifyContent="center" alignItems="center">
            <>
              <Document
                className="pdf"
                file={data.resume_path ? `http://localhost:3100${data.resume_path}` : `http://localhost:3100/public/resumes/sample.pdf`}
                onLoadSuccess={onDocumentLoadSuccess}
                loading={<LoadingPdf />}
                error={<ErrorPdf />}
              >
                {/* <Outline onItemClick={onItemClick} /> */}

                <Page pageNumber={pageNumber} width={950} />

              </Document>

              <Box w="300px">
                <Flex alignItems="center" justifyContent="space-around">
                  <Box>
                    <Button
                      bg="#fb5b1f"
                      color="white"
                      leftIcon={<ChevronLeftIcon />}
                      variant="solid"
                      disabled={pageNumber <= 1}
                      onClick={previousPage}
                      _hover={{
                        bg: "#ebedf0",
                        color: "#fb5b1f",
                        borderColor: "#fb5b1f"
                      }}
                      _active={{
                        bg: "#fb5b1f",
                        color: "#fff"
                      }}
                    >
                      prev
                    </Button>
                  </Box>
                  <Text textAlign="center">
                    Page {pageNumber || (numPages ? 1 : '--')} of {numPages || '--'}
                  </Text>
                  <Box>
                    <Button
                      bg="#fb5b1f"
                      color="white"
                      rightIcon={<ChevronRightIcon />}
                      variant="solid"
                      disabled={pageNumber >= numPages}
                      onClick={nextPage}
                      _hover={{
                        bg: "#ebedf0",
                        color: "#fb5b1f",
                        borderColor: "#fb5b1f"
                      }}
                      _active={{
                        bg: "#fb5b1f",
                        color: "#fff"
                      }}
                    >
                      next
                    </Button>
                  </Box>
                </Flex>
              </Box>
            </>
          </Flex>
        </GridItem>
      </Grid>
    </>
  )
}

/**
 * 
 * @type {import('next').GetServerSideProps}
 */
export const getServerSideProps = async (ctx) => {
  const data = await fetch(`http://localhost:3100/api/applicants/${ctx.params.applicant_id}`)
  const json = await data.json()

  console.log(json);
  return {
    props: {
      data: json.data
    }
  }
}

export default SingleApplicant