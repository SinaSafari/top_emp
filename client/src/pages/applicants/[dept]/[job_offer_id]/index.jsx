import React, { useState } from 'react'
import dynamic from "next/dynamic";
import { Text, Flex, Button, useToast, Spinner, Box } from '@chakra-ui/react'
import router, { useRouter } from 'next/router';
import Link from 'next/link'
import { StarIcon } from '@chakra-ui/icons'

const Board = dynamic(() => import('@asseinfo/react-kanban'), { ssr: false });

const statusesList = {
  applied: "applied",
  in_progress: "in_progress",
  accepted_by_hr: "accepted_by_hr",
  accepted_by_admin: "accepted_by_admin",
  first_interview: "first_interview",
  second_interview: "second_interview",
  third_interview: "third_interview",
  final_approval: "final_approval",
  talent_pool: "talent_pool",
  rejected: "rejected",
}



const ApplicantsForADepartments = ({ myBoardData }) => {
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const toast = useToast()

  const updateStatus = async (aplicantId, newStatus) => {
    try {
      setLoading(true)
      const res = await fetch(`http://localhost:3100/api/applicants/${aplicantId}`, {
        method: "PUT",
        body: JSON.stringify({ status: newStatus }),
        headers: {
          'Content-Type': 'application/json'
        },
      })
      toast({
        title: "Status Updated",
        description: `applicant updated to "${newStatus.split("_").map(i => i.charAt(0).toUpperCase() + i.slice(1)).join(" ")}"`,
        status: "success",
        duration: 3000,
        isClosable: true,
      })
    } catch (err) {
      toast({
        title: "network error",
        description: `status did not update. reload the page please`,
        status: "error",
        duration: 3000,
        isClosable: true,
      })
    } finally {
      setLoading(false)
    }
  }

  return (
    <>
      {/* <pre>
        <code>
          {JSON.stringify(statuses.data, null, 4)}
        </code>
      </pre> */}
      <div>
        <Flex alignItems="center" justifyContent="space-between" mb="5" mx="5">
          <Text fontSize="6xl" fontWeight="bold">{router.query.dept.toUpperCase()}</Text>
          <Box>
            <Button bg="#fb5b1f" color="white" mx="1" onClick={(e) => { e.preventDefault(); router.push(`/applicants/add/${12}`) }}>add applicant</Button>
            <Button bg="#fb5b1f" color="white" mx="1" onClick={(e) => { e.preventDefault(); router.push(`/settings`) }}>add status</Button>
          </Box>
        </Flex>
        <div>
          {loading && (
            <Box position="fixed" top="0" left="0" width="100vw" display="flex" justifyContent="center" alignItems="center" height="100vh" background="black" opacity="0.5">
              <Spinner
                thickness="4px"
                speed="0.65s"
                emptyColor="gray.200"
                color="#fb5b1f"
                size="xl"
              />
            </Box>
          )}
          <>
            <Board
              initialBoard={myBoardData}
              allowAddColumn={false}
              disableColumnDrag={false}
              allowRenameColumn={false}
              renderColumnHeader={(column, columnBag) => <CustomColumnRenderer column={column} columnBag={columnBag} />}
              renderCard={(card, dragging) => <CustomCard card={card} dragging={dragging} />}
              onCardDragEnd={(board, card, source, destination) => updateStatus(card.id, board.columns[destination.toColumnId].title)}
            />
          </>


        </div>
      </div>
      <style jsx>{`
      .react-kanban-card {
        width: 100%;
        background: white;
      }
      `}</style>
    </>
  )
}

const CustomCard = ({ card, dragging }) => (
  <Link href={`/applicants/${router.query.dept}/${router.query.job_offer_id}/${card.id}`}>
    <a>

      <div style={{ borderRadius: "8px" }} className={`react-kanban-card ${dragging ? 'react-kanban-card--dragging' : ''}`} >
        <span>
          <div className='react-kanban-card__title'>
            <span>
              {
                card.title.length > 20
                  ? `${card.title.substring(0, 20)} ...`
                  : card.title
              }
            </span>
          </div>
        </span>
        <div className='react-kanban-card__description'>
          {/* {card.description} */}
          <Box>
            Phone: {JSON.parse(card.description).phone}
          </Box>
          <Box>
            {/* TODO: add name */}
            email: <small>{JSON.parse(card.description).email}</small>
          </Box>
          <Flex flexDir={"row-reverse"} mt="5">
            <StarIcon verticalAlign="middle" color={"orange"} h="auto" />
            &nbsp;
            {JSON.parse(card.description).rate}
            &nbsp;

            {/* (out of 10) */}
          </Flex>
        </div>
        <div>
        </div>
      </div>
    </a>
  </Link>
)

const CustomColumnRenderer = ({ column, columnBag }) => (
  <div className="w-100 mb-3" style={{ borderRadius: "8px" }}>
    <Text textAlign="center" fontSize="3xl">
      {
        column
          .title
          .split("_")
          .map(i => i.charAt(0).toUpperCase() + i.slice(1)).join(" ")
      }
    </Text>
  </div>
)

/**
 * @type {import('next').GetServerSideProps} 
 */
export const getServerSideProps = async (ctx) => {




  const data = await fetch(`http://localhost:3100/api/joboffers/${ctx.params.job_offer_id}/applicants`)
  const json = await data.json()

  let statuses = await fetch("http://localhost:3100/api/statuses")
  statuses = await statuses.json()

  let columns = []

  /**
   * 
   * @param {Array} array_ref 
   * @param {Object} statusList 
   * @param {Array} data
   */
  const prepareColumnsBoilerPlate = (array_ref, statusList, data) => {
    Object.entries(statusList).forEach(([key, val], idx) => {
      array_ref = [
        ...array_ref,
        {
          id: idx,
          title: val.title,
          cards: [...data.filter(i => i.status == val.title).map(i => ({ id: i.id, title: i.fullname, description: JSON.stringify({ phone: i.phone, rate: i.rate, email: i.email }) }))]
        }
      ]
    })
    return array_ref
  }

  return {
    props: {
      myBoardData: { columns: [...prepareColumnsBoilerPlate(columns, statuses.data, json.data)] },
    }
  }
}

export default ApplicantsForADepartments
