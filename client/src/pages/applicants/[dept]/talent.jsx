import React from 'react'
import { useRouter } from 'next/router'


const TalentPool = () => {
  const router = useRouter()
  return (
    <div>
      Talent pool is here for {router.query.dept} department
    </div>
  )
}

export default TalentPool
