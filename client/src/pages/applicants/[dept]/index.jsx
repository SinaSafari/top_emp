// list of job offers
import { Box, Container, SimpleGrid, Text, Flex, Button } from '@chakra-ui/react'
import Link from 'next/link'
import { useRouter } from 'next/router'

const SingleApplicant = ({ data }) => {

  const router = useRouter()

  return (
    <>
      <Container maxW="container.lg">
        <Box mb={5}>
          <Flex alignItems="center" justifyContent="space-between">
            <Box>
              <Text fontSize={"6xl"}>
                {router.query.dept.toUpperCase()}
              </Text>
              <Text fontSize={"md"}>
                you can see the applicants for each job offer here.
              </Text>
            </Box>
            <Box>
              <Button
                bg="#fb5b1f"
                color="white"
                mx="1"
                px="10"
                onClick={(e) => {
                  e.preventDefault();
                  router.push(`/applicants/add/${1}`)
                }}
              >
                add
              </Button>
              <Button
                bg="#fb5b1f"
                color="white"
                mx="1"
                onClick={(e) => {
                  e.preventDefault();
                  router.push(`/applicants/${router.query.dept}/talent`)
                }}
              >
                talent pool
              </Button>
            </Box>
          </Flex>
        </Box>
        {data.job_offers.length ? (
          <>
            <SimpleGrid columns={2} spacing={10}>
              {data.job_offers.map(i => (
                <Box key={i.title} border="1px solid #fb5b1f" p={3} borderRadius={10}>
                  <Text fontSize="xl">{i.title}</Text>
                  <Text>{i.department.title}</Text>
                  <Flex alignItems="center" justifyContent="space-between">

                    <Text>
                      <span style={{ fontSize: "11px", verticalAlign: "middle" }}>
                        publish date:
                      </span>
                      &nbsp;
                      <span>
                        {new Date(i.created_at).toLocaleDateString()}
                      </span>
                    </Text>
                    <Link href={`/applicants/${router.query.dept}/${i.id}`}>
                      <a>
                        <Button
                          color="white"
                          bg="#fb5b1f"
                          border="1px solid #fb5b1f"
                          _hover={{
                            color: "#fb5b1f",
                            bg: "white",
                          }}
                        >
                          applicants
                        </Button>
                      </a>
                    </Link>
                  </Flex>
                </Box>
              ))}
            </SimpleGrid>
          </>
        ) : (
          <>
            <Flex minH={"30vh"} justifyContent="center" alignItems="center">
              <Text
                textAlign="center"
                fontSize="xl"
              >
                no active job offer
              </Text>
            </Flex>
          </>
        )}
      </Container>
    </>
  )
}

/**
 * 
 * @type {import('next').GetServerSideProps}
 */
export const getServerSideProps = async (ctx) => {
  const data = await fetch(`http://localhost:3100/api/joboffers?dept_id=${ctx.params.dept}`)
  const json = await data.json()
  return {
    props: {
      data: json
    }
  }
}


export default SingleApplicant