import React from 'react'
import { Box, Flex, Text, Table, Thead, Tr, Th, Td, Tbody, useColorModeValue, ButtonGroup, Button, Divider } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { Fragment } from 'react'
import Link from 'next/link'



const JobOffersOfADepartment = ({ data }) => {
  const router = useRouter()
  const header = ['id', 'created', 'actions'];

  const tableData = [
    { id: 1, name: "Daggy", created: "7 days ago" },
    { id: 2, name: "Anubra", created: "23 hours ago" },
    { id: 3, name: "Josef", created: "A few seconds ago" },
    { id: 4, name: "Sage", created: "A few hours ago" },
    { id: 5, name: "Daggy", created: "7 days ago" },
    { id: 6, name: "Anubra", created: "23 hours ago" },
    { id: 7, name: "Josef", created: "A few seconds ago" },
    { id: 8, name: "Sage", created: "A few hours ago" },
    { id: 9, name: "Daggy", created: "7 days ago" },
    { id: 10, name: "Anubra", created: "23 hours ago" },
    { id: 11, name: "Josef", created: "A few seconds ago" },
    { id: 12, name: "Sage", created: "A few hours ago" },
  ];

  const deleteJobOfferHandler = async (e, id) => {
    e.preventDefault()
    // TODO: adding
    console.log("deleting... whit id: ", id);
    // TODO: reloading the list
  }

  return (
    <>
      <Flex justifyContent="space-between" alignItems="center">
        <Text fontSize="6xl" fontWeight="bold">{router.query.dept.toUpperCase()}</Text>
        <Button
          bg="#fb5b1f"
          color="white"
          px="10"
          onClick={(e) => {
            e.preventDefault();
            router.push(`/job_offers/${router.query.dept}/create`)
          }}
        >
          add
        </Button>
      </Flex>
      <Box>
        <Flex
          w="full"
          bg="gray.100"
          p={50}
          alignItems="center"
          justifyContent="center"
        >
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th color="#fb5b1f">title</Th>
                <Th color="#fb5b1f" textAlign="center">No. applicant</Th>

                <Th color="#fb5b1f">creation time</Th>
                <Th color="#fb5b1f">actions</Th>
              </Tr>
            </Thead>
            <Tbody>
              {data.map(i => {
                return (

                  <Tr key={i.id} >
                    <Td>
                      <Link href={`/job_offers/${router.query.dept}/${i.id}`}>
                        <a>
                          <Text fontSize="xl">{i.title}</Text>
                        </a>
                      </Link>
                    </Td>
                    <Td textAlign="center">
                      {i.applicants.length}
                    </Td>
                    <Td>{new Date(i.created_at).toLocaleDateString()}</Td>
                    <Td>
                      <ActionBar id={i.id} />
                    </Td>
                  </Tr>
                )
              })}
            </Tbody>
          </Table>
        </Flex>
      </Box>
    </>
  )
}

const ActionBar = ({ id }) => {
  const onClickHandler = async (e) => {
    e.preventDefault()
    console.log("deleting id: " + id);
  }
  return (
    <ButtonGroup>
      <Button colorScheme="red" size="xs" onClick={onClickHandler}>
        delete
      </Button>
      {/* <Button colorScheme="teal" size="xs">
        Button {id}
      </Button> */}
    </ButtonGroup>
  )
}

/**
 * @type {import('next').GetServerSideProps} 
 */
export const getServerSideProps = async (ctx) => {
  const data = await fetch(`http://localhost:3100/api/joboffers?dept_id=${ctx.params.dept}`)
  const json = await data.json()
  return {
    props: {
      param: ctx.params.dept,
      data: json.job_offers
    }
  }
}

export default JobOffersOfADepartment
