import { useRouter } from 'next/router'

import React, { useEffect, useState } from 'react'
import { SimpleGrid, Box, Flex, Text, chakra, Stack, FormControl, FormLabel, Input, Textarea, Button, useColorModeValue, Select, ButtonGroup } from '@chakra-ui/react'
import { Editor } from '../../../components/common/Editor'

const SingleJobOffer = ({ data }) => {

  const router = useRouter()
  const [title, setTitle] = useState('')
  const [aboutRole, setAboutRole] = useState('')
  const [responsibilities, setResponsibilities] = useState('')
  const [requirements, setRequirements] = useState('')
  const [qoute, setQuote] = useState('Are you ready for the biggest challenge of your life?')
  const [status, setStatus] = useState('published')

  const [previewContent, setPreviewContent] = useState('')

  useEffect(() => {
    // TODO: initialize states with props for fruther updates
    const parsedData = JSON.parse(data.content)
    setTitle(data.title)
    setAboutRole(parsedData.aboutRole)
    setResponsibilities(parsedData.responsibilities)
    setRequirements(parsedData.requirements)

    setPreviewContent({
      title: data.title,
      aboutRole: parsedData.aboutRole,
      responsibilities: parsedData.responsibilities,
      requirements: parsedData.requirements,
    })
  }, [data])




  const submitHandler = (e) => {
    e.preventDefault()
    // TODO post request for this.
    console.log({ title, aboutRole, responsibilities, requirements, qoute, status });
  }

  const previewHandler = (e) => {
    e.preventDefault()
    // TODO: preview functionality
    setPreviewContent({
      title,
      aboutRole,
      responsibilities,
      requirements,
    })
  }


  return (
    <div>
      <SimpleGrid columns={2} spacing={5}>
        <Flex flexDir="column" borderRadius="10px" p="3">
          <chakra.form
            w={"100%"}
            method="POST"
            shadow="base"
            rounded={[null, "md"]}
            overflow={{ sm: "hidden" }}
          >
            <Stack
              px={4}
              py={5}
              bg={useColorModeValue("white", "gray.700")}
              spacing={6}
              p={{ sm: 6 }}
            >
              <div>
                <FormControl mb="5">
                  <FormLabel
                    htmlFor="email_address"
                    fontSize="sm"
                    fontWeight="md"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Job offer title
                  </FormLabel>
                  <Input
                    type="text"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    name="email_address"
                    id="email_address"
                    placeholder="the title will be shown in the list of offers."
                    autoComplete="email"
                    mt={1}
                    focusBorderColor="brand.400"
                    shadow="sm"
                    // size="sm"
                    w="full"
                    rounded="md"
                  />
                </FormControl>
                <FormControl id="email" mb="5">
                  <FormLabel
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    About the role
                  </FormLabel>
                  {/* <Textarea
                    value={aboutRole}
                    onChange={(e) => setAboutRole(e.target.value)}
                    placeholder="A brief description about the role and job offer."
                    mt={1}
                    rows={10}
                    shadow="sm"
                    // fontSize="md"
                    focusBorderColor="brand.400"
                  // fontSize={{ sm: "sm" }}
                  /> */}
                  {aboutRole && (
                    <Editor contentState={aboutRole} setContentState={setAboutRole} />
                  )}

                </FormControl>
                <FormControl id="email" mb="5">
                  <FormLabel
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Responsibilities
                  </FormLabel>
                  {/* <Textarea
                    value={responsibilities}
                    onChange={e => setResponsibilities(e.target.value)}
                    placeholder="list of responsibilities which the job offer includes."
                    mt={1}
                    rows={10}
                    shadow="sm"
                    focusBorderColor="brand.400"
                  // fontSize={{ sm: "sm" }}
                  /> */}
                  {responsibilities && (
                    <Editor contentState={responsibilities} setContentState={setResponsibilities} />
                  )}


                </FormControl>
                <FormControl id="email" mb="5">
                  <FormLabel
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Requirements
                  </FormLabel>
                  {/* <Textarea
                    value={requirements}
                    onChange={e => setRequirements(e.target.value)}
                    placeholder="minimum skills and abilities that is required for the job offer"
                    mt={1}
                    rows={10}
                    shadow="sm"
                    focusBorderColor="brand.400"
                  // fontSize={{ sm: "sm" }}
                  /> */}
                  {requirements && (
                    <Editor contentState={requirements} setContentState={setRequirements} />
                  )}


                </FormControl>
                <FormControl mb="5">
                  <FormLabel
                    htmlFor="email_address"
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Quote
                  </FormLabel>
                  <Input
                    value={qoute}
                    onChange={e => setQuote(e.target.value)}
                    type="text"
                    name="email_address"
                    id="email_address"
                    placeholder="quote that is used in the end of job offer description"
                    mt={1}
                    focusBorderColor="brand.400"
                    shadow="sm"
                    // size="sm"
                    w="full"
                    rounded="md"
                  />
                </FormControl>

                <FormControl mb="5">
                  <FormLabel
                    htmlFor="email_address"
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Status of job offer
                  </FormLabel>
                  <Select value={status} onChange={(e) => { console.log(e.target.value); setStatus(e.target.value) }}>
                    <option defaultValue disabled>status of job offer</option>
                    <option value="published">Published</option>
                    <option value="draft">Draft</option>
                  </Select>
                </FormControl>
              </div>
            </Stack>
            <Box
              px={{ base: 4, sm: 6 }}
              py={3}
              bg={useColorModeValue("gray.50", "gray.900")}
              textAlign="right"
            >
              <ButtonGroup>
                <Button
                  type="submit"
                  // colorScheme="brand"
                  onClick={previewHandler}
                  // background="#fb5b1f"
                  // color="white"
                  colorScheme="blue"
                  _focus={{ shadow: "" }}
                  fontWeight="md"
                >
                  Preview
                </Button>
                <Button
                  type="submit"
                  // colorScheme="brand"
                  onClick={submitHandler}
                  background="#fb5b1f"
                  color="white"
                  _focus={{ shadow: "" }}
                  fontWeight="md"
                >
                  Save
                </Button>
              </ButtonGroup>

            </Box>
          </chakra.form>
        </Flex>
        <Flex flexDir="column" background="white" borderRadius="10px" p="3">
          <Text fontSize="4xl" fontWeight="bold">Preview</Text>
          <Box border="1px solid rgba(0,0,0,0.1)" borderRadius="10px" h="100%">
            {previewContent && previewContent.aboutRole ? (
              <Box p="3" px="8">
                <Box mb="3">
                  <Text fontSize="4xl" fontWeight="bold">
                    {previewContent.title}
                  </Text>
                </Box>
                <Box mb="3">
                  <Text fontSize="2xl" fontWeight="bold">about role</Text>
                  {/* <Text>{previewContent.aboutRole}</Text> */}
                  <div dangerouslySetInnerHTML={{ __html: previewContent.aboutRole }}></div>
                </Box>
                <Box mb="3">
                  <Text fontSize="2xl" fontWeight="bold">responsibilities</Text>
                  {/* <Text>{previewContent.responsibilities.split("-").join("\r\n")}</Text> */}
                  <div dangerouslySetInnerHTML={{ __html: previewContent.responsibilities }}></div>
                </Box>
                <Box mb="3">
                  <Text fontSize="2xl" fontWeight="bold">requirements</Text>
                  {/* <Text>{previewContent.requirements.split("-").join("\r\n")}</Text> */}
                  <div dangerouslySetInnerHTML={{ __html: previewContent.requirements }}></div>
                </Box>
              </Box>
            ) : (
              <>
                <Flex justifyContent="center" alignItems="center" h="100%">
                  nothing to preview
                </Flex>
              </>
            )}
          </Box>
        </Flex>
      </SimpleGrid>
    </div>
  )
}

/**
 * @type {import('next').GetServerSideProps}
 */
export const getServerSideProps = async (ctx) => {
  const data = await fetch(`http://localhost:3100/api/joboffers/${ctx.query.id}`)
  const json = await data.json()
  return {
    props: {
      data: json.job_offer
    }
  }
}


export default SingleJobOffer
