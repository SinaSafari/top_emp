import React, { useState } from 'react'
import { useRouter } from 'next/router'
import {
  useColorModeValue,
  Input,
  Button,
  chakra,
  Textarea,
  Select,
  FormControl,
  FormHelperText,
  FormLabel,
  Box,
  Flex,
  GridItem,
  Stack,
  Text
} from '@chakra-ui/react'
import { Editor } from '../../../components/common/Editor'

// about the role
// responsibilities
// requirements
// qoute

const CreateJobOffer = ({ data }) => {
  const router = useRouter()

  const [title, setTitle] = useState('')
  const [aboutRole, setAboutRole] = useState('')
  const [responsibilities, setResponsibilities] = useState('')
  const [requirements, setRequirements] = useState('')
  const [qoute, setQuote] = useState('Are you ready for the biggest challenge of your life?')
  const [department, setDepartment] = useState(1)
  const [loading, setLoading] = useState(false)
  const [svrMsg, setSvrMsg] = useState('')
  const [jobOfferStatus, setjobOfferStatus] = useState('draft')

  const submitHandler = async (e) => {
    try {
      e.preventDefault()
      setLoading(true)
      const payload = {
        title,
        content: JSON.stringify({
          aboutRole,
          responsibilities,
          requirements
        }),
        qoute,
        department: department
      }
      const data = await fetch('http://localhost:3100/api/joboffers', {
        method: "POST",
        body: JSON.stringify(payload),
        headers: {
          "Content-Type": "application/json"
        }
      })

      if (data.status >= 200 && data.status <= 400) {
        setSvrMsg("something went wrong...")
        return
      }

      router.push(`/departments/${router.query.dept}`)

    } catch (err) {
      setSvrMsg('something went wrong...', err.toString())
      console.log(err.toString());

    } finally {
      setLoading(false)
    }
  }

  return (
    <Box bg={useColorModeValue("gray.50", "inherit")} p={10}>
      <Box mb="5">
        <Text fontSize="6xl" fontWeight="bold">{router.query.dept && router.query.dept.toUpperCase()}</Text>
        <Text>
          You are creating a job offer for&nbsp;
          <Text
            display="inline"
            fontSize="lg"
            fontWeight="bold"
          >
            {router.query.dept}
          </Text>
          &nbsp;department
        </Text>
      </Box>

      <Box>
        <Flex justifyContent="center" alignItems="center" >

          <chakra.form
            w={"100%"}
            method="POST"
            shadow="base"
            rounded={[null, "md"]}
            overflow={{ sm: "hidden" }}
          >
            <Stack
              px={4}
              py={5}
              bg={useColorModeValue("white", "gray.700")}
              spacing={6}
              p={{ sm: 6 }}
            >
              <div>
                <FormControl mb="5">
                  <FormLabel
                    htmlFor="email_address"
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Job offer title
                  </FormLabel>
                  <Input
                    type="text"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    name="email_address"
                    id="email_address"
                    placeholder="the title will be shown in the list of offers."
                    autoComplete="email"
                    mt={1}
                    focusBorderColor="brand.400"
                    shadow="sm"
                    w="full"
                    rounded="md"
                  />
                  {/* <Editor contentState={title} setContentState={setTitle} /> */}
                </FormControl>
                <FormControl id="email" mb="5">
                  <FormLabel
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    About the role
                  </FormLabel>
                  {/* <Textarea
                    value={aboutRole}
                    onChange={(e) => setAboutRole(e.target.value)}
                    placeholder="A brief description about the role and job offer."
                    mt={1}
                    rows={3}
                    shadow="sm"
                    focusBorderColor="brand.400"
                  // fontSize={{ sm: "sm" }}
                  /> */}
                  <Editor contentState={aboutRole} setContentState={setAboutRole} />

                </FormControl>
                <FormControl id="email" mb="5">
                  <FormLabel
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Responsibilities
                  </FormLabel>
                  {/* <Textarea
                    value={responsibilities}
                    onChange={e => setResponsibilities(e.target.value)}
                    placeholder="list of responsibilities which the job offer includes."
                    mt={1}
                    rows={3}
                    shadow="sm"
                    focusBorderColor="brand.400"
                  // fontSize={{ sm: "sm" }}
                  /> */}
                  <Editor contentState={responsibilities} setContentState={setResponsibilities} />

                </FormControl>
                <FormControl id="email" mb="5">
                  <FormLabel
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Requirements
                  </FormLabel>
                  {/* <Textarea
                    value={requirements}
                    onChange={e => setRequirements(e.target.value)}
                    placeholder="minimum skills and abilities that is required for the job offer"
                    mt={1}
                    rows={3}
                    shadow="sm"
                    focusBorderColor="brand.400"
                  // fontSize={{ sm: "sm" }}
                  /> */}
                  <Editor contentState={requirements} setContentState={setRequirements} />


                </FormControl>
                <FormControl mb="5">
                  <FormLabel
                    htmlFor="email_address"
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    Quote
                  </FormLabel>
                  <Input
                    value={qoute}
                    onChange={e => setQuote(e.target.value)}
                    type="text"
                    name="email_address"
                    id="email_address"
                    placeholder="quote that is used in the end of job offer description"
                    mt={1}
                    focusBorderColor="brand.400"
                    shadow="sm"
                    // size="sm"
                    w="full"
                    rounded="md"
                  />
                </FormControl>

                <FormControl mb="5">
                  <FormLabel
                    htmlFor="email_address"
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    depertment
                  </FormLabel>
                  <Select value={department} onChange={e => setDepartment(e.target.value)}>
                    {data.departments && data.departments.map((i, idx) => (
                      <option
                        key={idx}
                        name={i.title}
                        defaultChecked={idx == 0}
                        value={i.id}
                      >
                        {i.title.charAt(0).toUpperCase() + i.title.slice(1)}
                      </option>
                    ))}
                  </Select>
                </FormControl>
                <FormControl mb="5">
                  <FormLabel
                    htmlFor="email_address"
                    fontSize="xl"
                    fontWeight="bold"
                    color={useColorModeValue("gray.700", "gray.50")}
                  >
                    status
                  </FormLabel>
                  <Select value={jobOfferStatus} onChange={e => setjobOfferStatus(e.target.value)}>
                    {/* {data.departments && data.departments.map((i, idx) => (
                      <option
                        key={idx}
                        name={i.title}
                        defaultChecked={idx == 0}
                        value={i.id}
                      >
                        {i.title.charAt(0).toUpperCase() + i.title.slice(1)}
                      </option>
                    ))} */}
                    <option

                      defaultChecked={true}
                      value={"draft"}
                    >
                      draft
                    </option>
                    <option

                      // defaultChecked={true}
                      value={"published"}
                    >
                      published
                    </option>
                  </Select>
                </FormControl>
              </div>
            </Stack>
            <Box
              px={{ base: 4, sm: 6 }}
              py={3}
              bg={useColorModeValue("gray.50", "gray.900")}
              textAlign="right"
            >
              <Button
                type="submit"
                // colorScheme="brand"
                onClick={submitHandler}
                background="#fb5b1f"
                color="white"
                _focus={{ shadow: "" }}
                fontWeight="md"
                disabled={loading}
              >
                {loading ? (
                  <>
                    Please wait
                  </>
                ) : (
                  <>
                    Add
                  </>
                )}
                {/* Add */}
              </Button>
            </Box>
          </chakra.form>
        </Flex>
        {/* </SimpleGrid> */}
      </Box>
      <Box visibility={{ base: "hidden", sm: "visible" }} aria-hidden="true">
        <Box py={5}>
          <Box
            borderTop="solid 1px"
            borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
          ></Box>
        </Box>
      </Box>
    </Box>
  )
}


export const getServerSideProps = async (ctx) => {
  const data = await fetch("http://localhost:3100/api/departments")
  const json = await data.json()

  return {
    props: {
      data: json
    }
  }
}

export default CreateJobOffer
