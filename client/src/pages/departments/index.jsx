import { Box, Center, Container, Flex, Heading, HStack, SimpleGrid, Stack } from '@chakra-ui/layout'
import { useColorModeValue, Text, Avatar } from '@chakra-ui/react'
import { chakra } from '@chakra-ui/system'
import React from 'react'
import Link from 'next/link'
import Image from 'next/image'

import { DepartmentsList } from '../../content/departments'



/**
 * @type {import('next').NextPage}
 */
const Page = ({ param, data }) => {
  return (
    <>
      <Container maxW="container.xl">
        <SimpleGrid columns={2} spacing={5}>
          {data.departments.map(i => (
            <Center key={i.id} py={6}>
              <Box
                maxW={'445px'}
                w={'full'}
                bg={useColorModeValue('white', 'gray.900')}
                boxShadow={'2xl'}
                rounded={'md'}
                p={6}
                overflow={'hidden'}>
                <Box
                  h={'100px'}
                  bg={'gray.100'}
                  mt={-6}
                  mx={-6}
                  mb={6}
                  pos={'relative'}>
                  <Image
                    src={
                      'https://images.unsplash.com/photo-1515378791036-0648a3ef77b2?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80'
                    }
                    layout={'fill'}
                  />
                </Box>
                <Stack>
                  <Heading
                    color={useColorModeValue('gray.700', 'white')}
                    fontSize={'xl'}
                    fontFamily={'body'}>
                    {i.title.toUpperCase()}
                  </Heading>
                </Stack>
                <Stack mt={6} direction={'row-reverse'} spacing={4} align={'center'} >
                  <Stack direction={'column'} spacing={0} fontSize={'sm'}>
                    <Link href={`/departments/${i.title}`}>
                      <a>
                        details
                      </a>
                    </Link>
                  </Stack>
                </Stack>
              </Box>
            </Center>
          ))}
        </SimpleGrid>
      </Container>
    </>
  )
}

/**
 * @type {import('next').GetServerSideProps} 
 */
export const getServerSideProps = async (ctx) => {
  const data = await fetch("http://localhost:3100/api/departments")
  const json = await data.json()

  return {
    props: {
      // param: ctx.params.dept
      data: json
    }
  }
}

export default Page
