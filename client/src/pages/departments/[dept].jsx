import React from 'react'
import { CustomTable } from '../../components/common/table'

/**
 * @type {import('next').NextPage}
 */
const Page = ({ param }) => {

  const tableData = [
    { name: "Daggy", created: "7 days ago" },
    { name: "Anubra", created: "23 hours ago" },
    { name: "Josef", created: "A few seconds ago" },
    { name: "Sage", created: "A few hours ago" },
    { name: "Daggy", created: "7 days ago" },
    { name: "Anubra", created: "23 hours ago" },
    { name: "Josef", created: "A few seconds ago" },
    { name: "Sage", created: "A few hours ago" },
    { name: "Daggy", created: "7 days ago" },
    { name: "Anubra", created: "23 hours ago" },
    { name: "Josef", created: "A few seconds ago" },
    { name: "Sage", created: "A few hours ago" },
  ];


  return (
    <div>
      <p>{param.toUpperCase()} department.</p>
      {/* <CustomTable data={tableData} /> */}

    </div>
  )
}

/**
 * @type {import('next').GetServerSideProps} 
 */
export const getServerSideProps = async (ctx) => {
  return {
    props: {
      param: ctx.params.dept
    }
  }
}

export default Page
