import axios from "axios";
import { baseUrl } from '../serviceContainer'

export const getAllEvents = async () => {
  try {
    const { data } = await axios.get(`${baseUrl}/events`)
    return data
  } catch (err) {
    console.log(data)
  }
}