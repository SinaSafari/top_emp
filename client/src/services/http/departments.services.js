import axios from 'axios'
import { baseUrl } from '../serviceContainer'

export const getListOfAllDepartments = async () => {
  try {
    const { data } = await axios.get(`${baseUrl}/departments`)
    return data
  } catch (err) {
    console.log(err)
  }
}