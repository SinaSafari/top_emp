import axios from 'axios'
import { baseUrl } from '../serviceContainer'

export const loginRequset = async (body) => {
  try {
    console.log(body)
    const { status, data } = await axios.post(`${baseUrl}/api/auth/login`, body)
    console.log(data)
    if (status == 200) {
      return {
        success: true,
        message: "data",
        data
      }
    }
    else if (status == 400) {
      return {
        success: false,
        message: "user not found...",
        data
      }
    } else if (status == 401) {
      return {
        success: false,
        message: "email or password is wrong...",
        data
      }
    }
  } catch (err) {
    return {
      success: false,
      message: "something went wrong...",
      data: err.toString()
    }
  }
}