const { getListOfAllDepartments } = require('./http/departments.services')
const { getAllEvents } = require('./http/events.services')
const { getAllJobOffers } = require('./http/joboffers.services')
const { getToken, setToken } = require('./storage/index')
const { loginRequset } = require('./http/auth.services')

export const baseUrl = "http://localhost:3100";

const httpServices = () => ({
  getListOfAllDepartments,
  getAllJobOffers,
  getAllEvents,
  loginRequset,
})

const storageServices = () => ({
  setToken,
  getToken
})

export const ServiceContainer = {
  http: httpServices(),
  storage: storageServices(),
}

