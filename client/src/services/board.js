
export const Statuses = {
  IN_PROGRESS: "IN_PROGRESS",
  HR_ACCEPTED: "HR_ACCEPTED",
  ADMIN_ACCEPTED: "ADMIN_ACCEPTED",
  FIRST_INTERVIEW: "FIRST_INTERVIEW",
  SECOND_INTERVIEW: "SECOND_INTERVIEW",
  THIRD_INTERVIEW: "THIRD_INTERVIEW",
  FINAL_APPROVAL: "FINAL_APPROVAL"
}

/**
 * 
 * @param {string} name 
 */
export const ColumnNameGenerator = (name) => {
  return name
    .split("_")
    .map(n => n.toLocaleLowerCase())
    .map(n => n.charAt(0).toUpperCase() + n.slice(1))
    .join(" ")
}

const columnFactory = (item) => ({
  id: item.id,
  title: item.email || "unknown",
  description: "not specified"
})

/**
 * 
 * @param {Array<any>} rawData 
 */
export const boardDataHandler = (rawData) => {
  let in_progress_column = []
  let hr_accepted_column = []
  let admin_accepted_column = []
  let first_interview_column = []
  let second_interview_column = []
  let third_interview_column = []
  let final_approval_column = []

  rawData.map(i => {
    switch (i.status) {
      case Statuses.IN_PROGRESS.toLowerCase():
        in_progress_column.push(columnFactory(i))
        break
      case Statuses.HR_ACCEPTED:
        hr_accepted_column.push(columnFactory(i))
        break
      case Statuses.ADMIN_ACCEPTED.toLowerCase():
        admin_accepted_column.push(columnFactory(i))
        break
      case Statuses.FIRST_INTERVIEW.toLowerCase():
        first_interview_column.push(columnFactory(i))
        break
      case Statuses.SECOND_INTERVIEW.toLowerCase():
        second_interview_column.push(columnFactory(i))
        break
      case Statuses.THIRD_INTERVIEW.toLowerCase():
        third_interview_column.push(columnFactory(i))
        break
      case Statuses.FINAL_APPROVAL.toLowerCase():
        final_approval_column.push(columnFactory(i))
        break
    }
  })
  return {
    [Statuses.IN_PROGRESS]: in_progress_column,
    [Statuses.HR_ACCEPTED]: hr_accepted_column,
    [Statuses.ADMIN_ACCEPTED]: admin_accepted_column,
    [Statuses.FIRST_INTERVIEW]: first_interview_column,
    [Statuses.SECOND_INTERVIEW]: second_interview_column,
    [Statuses.THIRD_INTERVIEW]: third_interview_column,
    [Statuses.FINAL_APPROVAL]: final_approval_column,
  }
}


export const boardGenerator = (rawData) => {
  const board = {
    columns: [],
  }

  const data = boardDataHandler(rawData)

  for (let [index, [key, value]] of Object.entries(Statuses)) {
    board.columns.push({
      id: index,
      title: ColumnNameGenerator(value),
      cards: data[key]
    })
  }

  return board
}