const base_url = "http://localhost:3000"

export const login = async (email, password) => {
  try {
    const res = await fetch(
      `${base_url}/auth/login`,
      {
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ email, password })
      }
    )
    const json = await res.json()
    return json
  } catch (err) {
    return err
  }
}