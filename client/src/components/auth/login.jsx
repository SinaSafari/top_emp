import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Center,
  Button,
  useColorModeValue,
} from '@chakra-ui/react';
import { useState } from 'react'
import { useCtxState, useCtxDispatch } from '../../context/AppContext'
import { loginRequest } from '../../context/actions'
import { useRouter } from 'next/router';
import { useStore } from '../../store/useStore'
import { useSession, signIn, signOut } from "next-auth/react"


export function LoginForm() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const router = useRouter()
  const state = useCtxState()
  const dispatch = useCtxDispatch()
  const [loading, setLoading] = useState(false)
  const [svrMsg, setSvrMsg] = useState("")

  const { user, token } = useStore(state => state)


  const onSubmitHandler = async () => {
    try {
      setLoading(true)
      // let res = await fetch(
      //   "http://localhost:3100/api/auth/login",
      //   {
      //     method: "POST",
      //     headers: {
      //       "Content-Type": "application/json",
      //     },
      //     body: JSON.stringify({
      //       email,
      //       password,
      //     })
      //   }
      // )
      // res = await res.json()
      // console.log(res);

      // useStore.setState({ user: res.user, token: res.token });
      // localStorage.setItem("auth", JSON.stringify(res))
      // router.push("/dashboard");
      signIn('credentials', { email, password, redirect: true, callbackUrl: "http://localhost:3000/dashboard", })

    } catch (err) {
      setSvrMsg("somehting went wrong...");
    } finally {
      setLoading(false);
    }
  }

  return (
    <Flex
      minH={'100vh'}
      // h="96"
      align={'center'}
      justify={'center'}
      bg={useColorModeValue('gray.50', 'gray.800')}>
      <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
        <Box
          rounded={'lg'}
          width={"400px"}
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow={'lg'}
          p={8}>
          <Center mb="10">
            <img src="/logo-top-fa.svg" width="200px" />
          </Center>
          <Stack spacing={4}>
            <FormControl id="email">
              <FormLabel>
                Email address
              </FormLabel>
              <Input
                type="email"
                placeholder="enter your email address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormControl>
            <FormControl id="password">
              <FormLabel>
                Password
              </FormLabel>
              <Input
                type="password"
                placeholder="enter your password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>
            <Stack spacing={10}>
              <Button
                onClick={onSubmitHandler}
                bg={'blue.400'}
                color={'white'}
                _hover={{
                  bg: 'blue.500',
                }}>
                {state.loading ? (<>loading...</>) : (<>signin</>)}
                {/* Sign in */}
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Stack>
    </Flex>
  );
}