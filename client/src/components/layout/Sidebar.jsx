import {
    Avatar,
    Box,
    Collapse,
    Drawer,
    DrawerContent,
    DrawerOverlay,
    Flex,
    Icon,
    IconButton,
    Input,
    InputGroup,
    InputLeftElement,
    Text,
    useColorModeValue,
    useDisclosure,
    Button,
    Badge,
} from "@chakra-ui/react";
import { FaBell, FaBuilding, FaCalendarCheck, FaEnvelopeOpen } from "react-icons/fa";
import { BsGearFill } from "react-icons/bs";
import { FiMenu, FiSearch } from "react-icons/fi";
import { HiCode, HiCollection } from "react-icons/hi";
import { MdHome, MdKeyboardArrowRight } from "react-icons/md";
import React, { useState } from "react";
import NextLink from 'next/link'
import Image from 'next/image'
import { useStore } from "../../store/useStore";
import { useRouter } from "next/router";
import { BellIcon } from '@chakra-ui/icons'
import { signOut } from "next-auth/react";

export default function Sidebar({ children }) {
    const sidebar = useDisclosure();

    const { user, token } = useStore(s => s)
    const router = useRouter()

    const logoutHandler = (e) => {
        e.preventDefault()
        // useStore.setState({ user: {}, token: "" })
        // localStorage.removeItem("auth")
        // router.replace("/login")
        signOut({ redirect: true, callbackUrl: "http://localhost:3000/login" })
    }


    return (
        <Box
            as="section"
            bg={useColorModeValue("gray.50", "gray.700")}
            minH="100vh"
        >
            <SidebarContent display={{ base: "none", md: "unset" }} />
            <Drawer
                isOpen={sidebar.isOpen}
                onClose={sidebar.onClose}
                placement="left"
            >
                <DrawerOverlay />
                <DrawerContent>
                    <SidebarContent w="full" borderRight="none" />
                </DrawerContent>
            </Drawer>
            <Box ml={{ base: 0, md: 60 }} transition=".3s ease">
                <Flex
                    as="header"
                    align="center"
                    justify="space-between"
                    w="full"
                    px="4"
                    bg={useColorModeValue("white", "gray.800")}
                    borderBottomWidth="1px"
                    borderColor={useColorModeValue("inherit", "gray.700")}
                    h="14"
                >
                    <IconButton
                        aria-label="Menu"
                        display={{ base: "inline-flex", md: "none" }}
                        onClick={sidebar.onOpen}
                        icon={<FiMenu />}
                        size="sm"
                    />
                    <Flex w="96" display={{ base: "none", md: "flex" }} alignItems="center">
                        <Text fontSize="xl">you are logged in as: <Badge variant="subtle">{user.username ? user.username : user.email}</Badge></Text>
                        <Box px={3}>
                            <Button onClick={logoutHandler} fontWeight="light" border="1px solid red" color="red" bgColor="white">logout</Button>
                        </Box>
                    </Flex>
                    <Flex align="center">
                        <img src="/logo-top-fa.svg" alt="top" width="100" height="100" />
                        <BellIcon boxSize={8} />
                    </Flex>
                </Flex>
                <Box as="main" p="4">
                    {children}
                </Box>
            </Box>
        </Box>
    );
}



const NavItem = (props) => {
    const { icon, children, href, ...rest } = props;
    return (<>
        {href ? (
            <>
                <NextLink href={href}>
                    <Flex
                        align="center"
                        px="4"
                        pl="4"
                        py="3"
                        cursor="pointer"
                        // eslint-disable-next-line react-hooks/rules-of-hooks
                        color={useColorModeValue("inherit", "gray.400")}
                        _hover={{
                            // eslint-disable-next-line react-hooks/rules-of-hooks
                            bg: "#fb5b1f",
                            // eslint-disable-next-line react-hooks/rules-of-hooks
                            color: "white",
                        }}
                        role="group"
                        fontWeight="semibold"
                        transition=".15s ease"
                        {...rest}
                    >
                        {icon && (
                            <Icon
                                mr="2"
                                boxSize="4"
                                color="#fb5b1f"
                                _groupHover={{
                                    // eslint-disable-next-line react-hooks/rules-of-hooks
                                    color: "white",
                                }}
                                as={icon}
                            />
                        )}
                        {children}
                    </Flex>
                </NextLink>
            </>
        ) : (
            <>
                <Flex
                    align="center"
                    px="4"
                    pl="4"
                    py="3"
                    cursor="pointer"
                    // eslint-disable-next-line react-hooks/rules-of-hooks
                    color={useColorModeValue("inherit", "gray.400")}
                    _hover={{
                        // eslint-disable-next-line react-hooks/rules-of-hooks
                        bg: "#fb5b1f",
                        // eslint-disable-next-line react-hooks/rules-of-hooks
                        color: "white",
                    }}
                    role="group"
                    fontWeight="semibold"
                    transition=".15s ease"
                    {...rest}
                >
                    {icon && (
                        <Icon
                            mr="2"
                            boxSize="4"
                            color="#fb5b1f"
                            _groupHover={{
                                // eslint-disable-next-line react-hooks/rules-of-hooks
                                color: "white",
                            }}
                            as={icon}
                        />
                    )}
                    {children}
                </Flex>
            </>
        )}

    </>
    );
};


const SidebarContent = (props) => {

    const jobOfferToggle = useDisclosure()
    const applicantsToggle = useDisclosure()

    // TODO: selected item should be shown
    const [selected, setSelected] = useState('')

    return (
        <Box
            as="nav"
            pos="fixed"
            top="0"
            left="0"
            zIndex="sticky"
            h="full"
            pb="10"
            overflowX="hidden"
            overflowY="auto"
            bg={useColorModeValue("white", "gray.800")}
            borderColor={useColorModeValue("inherit", "gray.700")}
            borderRightWidth="1px"
            w="60"
            {...props}
        >
            <Flex px="4" py="5" align="center">
                <Box borderRadius="10" bg="#fb5b1f" w="100%" p="2">
                    <Text
                        fontSize="xl"
                        ml="2"
                        color="white"
                        fontWeight="semibold"
                        textAlign="center"

                    >
                        TopRecruiter
                    </Text>
                </Box>


            </Flex>
            <Flex
                direction="column"
                as="nav"
                fontSize="sm"
                color="gray.600"
                aria-label="Main Navigation"
            >
                <NavItem href="/dashboard" icon={MdHome}>
                    Home
                </NavItem>
                <NavItem href="/events" icon={FaCalendarCheck}>
                    Events
                </NavItem>
                <NavItem href="/departments" icon={FaBuilding}>
                    departments
                </NavItem>
                <NavItem icon={HiCode} onClick={jobOfferToggle.onToggle} >
                    job offers
                    <Icon
                        as={MdKeyboardArrowRight}
                        ml="auto"
                        transform={jobOfferToggle.isOpen && "rotate(90deg)"}
                    />
                </NavItem>
                <Collapse in={jobOfferToggle.isOpen}>
                    <NavItem pl="12" py="2" href={`/job_offers/${"tech"}`}>
                        Tech
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/job_offers/${"product"}`}>
                        Product
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/job_offers/${"hr"}`}>
                        HR
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/job_offers/${"marketing"}`}>
                        Marketing
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/job_offers/${"finance"}`}>
                        Finance
                    </NavItem>
                </Collapse>


                <NavItem icon={FaEnvelopeOpen} onClick={applicantsToggle.onToggle} >
                    applicants
                    <Icon
                        as={MdKeyboardArrowRight}
                        ml="auto"
                        transform={applicantsToggle.isOpen && "rotate(90deg)"}
                    />
                </NavItem>
                <Collapse in={applicantsToggle.isOpen}>
                    <NavItem pl="12" py="2" href={`/applicants/${"tech"}`}>
                        Tech
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/applicants/${"product"}`}>
                        Product
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/applicants/${"hr"}`}>
                        HR
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/applicants/${"marketing"}`}>
                        Marketing
                    </NavItem>
                    <NavItem pl="12" py="2" href={`/applicants/${"finance"}`}>
                        Finance
                    </NavItem>
                </Collapse>
                <NavItem icon={BsGearFill} href={`/settings`}>Settings</NavItem>
            </Flex>
        </Box>
    )
};