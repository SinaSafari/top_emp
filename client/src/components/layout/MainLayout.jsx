import React, { useEffect } from 'react'
import Sidebar from './Sidebar'
import { Box } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { LoginLayout } from './LoginLayout'
import { useStore } from '../../store/useStore'

export const MainLayout = ({ children }) => {
  const router = useRouter()
  const { user, token } = useStore(s => s)

  const RedirectToLogin = () => {
    const router = useRouter()
    useEffect(() => {
      router.push("/login")
    }, [router])
    return (<></>)
  }

  console.log(user);

  return (
    <>
      {router.pathname.includes('login') ? (
        <>
          <LoginLayout>
            {children}
          </LoginLayout>
        </>
      ) : (
        <>
          {/* {user && Object.entries(user).length <= 0 || !token
            ? (
              <>
                <RedirectToLogin />
              </>
            ) : ( */}
          <>
            <Sidebar>
              <Box h="100%" as="main">
                {children}
              </Box>
            </Sidebar>
          </>
          {/* )} */}
        </>
      )}
    </>
  )
}
