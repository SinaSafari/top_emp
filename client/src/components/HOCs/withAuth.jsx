import React from 'react'
import { useRouter } from 'next/router'

/**
 * 
 * @param {import('react').FC | import('react').Component} WrappedComponent 
 * @returns 
 */
// eslint-disable-next-line react/display-name
export const withAuth = (WrappedComponent) => (props) => {

  if (window !== undefined) {

    const router = useRouter()
    const token = localStorage.getItem('wt')

    if (!token) {
      router.push("/")
    } else {
      return <WrappedComponent {...props} />
    }
  }
}
