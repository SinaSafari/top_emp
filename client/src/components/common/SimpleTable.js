import { Flex, Table, Thead, Tr, Th, Td, Tbody, useColorModeValue, ButtonGroup, Button } from '@chakra-ui/react'
import { Fragment } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

export function SimpleTable() {
  const router = useRouter()
  const header = ['id', 'created', 'actions'];
  const data = [
    { id: 1, name: 'Daggy', created: '7 days ago' },
    { id: 2, name: 'Anubra', created: '23 hours ago' },
    { id: 3, name: 'Josef', created: 'A few seconds ago' },
    { id: 4, name: 'Sage', created: 'A few hours ago' },
  ];
  return (
    <Flex
      w="full"
      bg="gray.100"
      p={50}
      alignItems="center"
      justifyContent="center"
    >
      <Table
        w="full"
        bg={useColorModeValue('white', 'gray.800')}
        display={{
          base: 'block',
          md: 'table',
        }}
        sx={{
          '@media print': {
            display: 'table',
          },
        }}
      >
        <Thead
          display={{
            base: 'none',
            md: 'table-header-group',
          }}
          sx={{
            '@media print': {
              display: 'table-header-group',
            },
          }}
        >
          <Tr>
            {header.map((x) => (
              <Th key={x}>{x}</Th>
            ))}
          </Tr>
        </Thead>


        <Tbody
          display={{
            base: 'block',
            lg: 'table-row-group',
          }}
          sx={{
            '@media print': {
              display: 'table-row-group',
            },
          }}
        >
          {data.map((token, tid) => {
            return (

              <Tr
                key={tid}
                display={{
                  base: 'grid',
                  md: 'table-row',
                }}
                sx={{
                  '@media print': {
                    display: 'table-row',
                  },
                  gridTemplateColumns: 'minmax(0px, 35%) minmax(0px, 65%)',
                  gridGap: '10px',
                }}
              >

                {Object.keys(token).map((x) => {
                  return (
                    <Fragment key={`${tid}${x}`}>
                      <Td
                        display={{
                          base: 'table-cell',
                          md: 'none',
                        }}
                        sx={{
                          '@media print': {
                            display: 'none',
                          },
                          textTransform: 'uppercase',
                          color: useColorModeValue('gray.400', 'gray.400'),
                          fontSize: 'xs',
                          fontWeight: 'bold',
                          letterSpacing: 'wider',
                          fontFamily: 'heading',
                        }}
                      >
                        <Link href={`/job_offers/${router.query.dept}/${token.id}`}>
                          <a>{x}</a>
                        </Link>
                      </Td>
                      <Td
                        color={useColorModeValue('gray.500')}
                        fontSize="md"
                        fontWeight="hairline"
                      >
                        <Link href={`/job_offers/${router.query.dept}/${token.id}`}>
                          <a>{token[x]}</a>
                        </Link>

                      </Td>
                    </Fragment>
                  );
                })}
                <Td
                  display={{
                    base: 'table-cell',
                    md: 'none',
                  }}
                  sx={{
                    '@media print': {
                      display: 'none',
                    },
                    textTransform: 'uppercase',
                    color: useColorModeValue('gray.400', 'gray.400'),
                    fontSize: 'xs',
                    fontWeight: 'bold',
                    letterSpacing: 'wider',
                    fontFamily: 'heading',
                  }}
                >
                  Actions
                </Td>


                <Td>
                  <ButtonGroup variant="solid" size="sm" spacing={3}>
                    <Button
                      colorScheme="red"
                      variant="outline"
                    // icon={<BsFillTrashFill />}
                    >delete</Button>
                  </ButtonGroup>
                </Td>
              </Tr>

            );
          })}
        </Tbody>
      </Table>
    </Flex>
  );
}
