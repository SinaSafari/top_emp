import React from 'react'
import { useEditor, EditorContent } from '@tiptap/react'
import StarterKit from '@tiptap/starter-kit'
// import { defaultExtensions } from "@tiptap/starter-kit";

// import './styles.scss'
import { Button, ButtonGroup, Box } from '@chakra-ui/react'


const MenuBar = ({ editor }) => {
  if (!editor) {
    return null
  }

  return (
    <>
      <ButtonGroup mx={"4"}>
        <Button
          onClick={() => editor.chain().focus().toggleBulletList().run()}
          className={editor.isActive('bulletList') ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          bullet list
        </Button>
        <Button
          onClick={() => editor.chain().focus().toggleOrderedList().run()}
          className={editor.isActive('orderedList') ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          ordered list
        </Button>
      </ButtonGroup>


      <ButtonGroup mx={"4"} >
        <Button
          onClick={() => editor.chain().focus().toggleBlockquote().run()}
          className={editor.isActive('blockquote') ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          blockquote
        </Button>


        <Button
          onClick={() => editor.chain().focus().toggleBold().run()}
          className={editor.isActive('bold') ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          bold
        </Button>

      </ButtonGroup>


      <ButtonGroup mx={"4"}>
        <Button
          onClick={() => editor.chain().focus().toggleHeading({ level: 1 }).run()}
          className={editor.isActive('heading', { level: 1 }) ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          h1
        </Button>
        <Button
          onClick={() => editor.chain().focus().toggleHeading({ level: 2 }).run()}
          className={editor.isActive('heading', { level: 2 }) ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          h2
        </Button>
        <Button
          onClick={() => editor.chain().focus().toggleHeading({ level: 3 }).run()}
          className={editor.isActive('heading', { level: 3 }) ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          h3
        </Button>
        <Button
          onClick={() => editor.chain().focus().toggleHeading({ level: 4 }).run()}
          className={editor.isActive('heading', { level: 4 }) ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          h4
        </Button>
        <Button
          onClick={() => editor.chain().focus().toggleHeading({ level: 5 }).run()}
          className={editor.isActive('heading', { level: 5 }) ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          h5
        </Button>
        <Button
          onClick={() => editor.chain().focus().toggleHeading({ level: 6 }).run()}
          className={editor.isActive('heading', { level: 6 }) ? 'is-active' : ''}
          border={"1px solid #fb5a1f"}
          color={"#fb5a1f"}
          size='xs'
        >
          h6
        </Button>
      </ButtonGroup>



    </>
  )
}

export const Editor = ({ contentState, setContentState }) => {
  const editor = useEditor({
    extensions: [
      StarterKit,
    ],
    // extensions: defaultExtensions(),
    content: contentState,
    onUpdate: ({ editor }) => {
      setContentState((prev) => editor.getHTML())
    },
  })

  return (
    <div>
      <Box p={"5"} minH={"200px"} border={"1px solid grey"} borderRadius={"10px"}>
        <MenuBar editor={editor} />
        <Box mt={"5"}>
          <EditorContent editor={editor} style={{ minWidth: "200px", }} />
        </Box>
      </Box>
    </div>
  )
}

