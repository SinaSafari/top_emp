import React, { useState, useEffect } from 'react'
import { SettingsBody } from '../settingsBody'
import {
  Text,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
  Select,
  Tabs,
  TabList,
  TabPanels,
  TabPanel,
  Tab,
  Box,
  Flex,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Divider,
  Spinner,
  IconButton,
} from "@chakra-ui/react";
import { RepeatIcon } from '@chakra-ui/icons'

export const RolesSetting = () => {
  const body = [
    {
      id: 1,
      label: "Roles",
      component: <Flex flexDir="column" justifyContent="space-between" >
        <Box mb="5">
          <AddRole />
        </Box>
        <Box my="5">
          <RolesList />
        </Box>
      </Flex>
    }
  ]
  return (
    <SettingsBody data={body} />
  )
}


const RolesList = () => {

  const [rolesList, setRolesList] = useState([])
  const [loading, setLoading] = useState(false)
  const [svrMsg, setSvrMsg] = useState('')

  const loadData = async () => {
    try {
      setLoading(true)
      let res = await fetch("http://localhost:3100/api/roles")
      res = await res.json()
      setRolesList(res.data)
    } catch (err) {
      setSvrMsg("something went wrong")
    } finally {
      setLoading(false)
    }
  }

  const deleteHandler = async (e, id) => {
    try {
      e.preventDefault()
      await fetch(`http://localhost:3100/api/roles/${id}`, { headers: { "Content-Type": "application/json" }, method: "DELETE" })
    } catch (err) {

    }
  }


  useEffect(() => {
    loadData()
  }, [])



  return (
    <>
      <Flex justifyContent="space-between" alignItems="center">
        <Text fontSize="5xl" fontWeight="bold" mb="5">Roles List</Text>
        <IconButton
          onClick={loadData}
          aria-label="Refresh"
          background="#fb5b1f"
          color={"white"}
          icon={<RepeatIcon />}
        />
      </Flex>
      {svrMsg && <Text color="red">{svrMsg}</Text>}
      {loading ? (
        <Box>
          <Spinner />
        </Box>
      ) : (
        <>
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>
                  id
                </Th>
                <Th>
                  title
                </Th>
                <Th>
                  actions
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {rolesList.map((i, idx) => (
                <Tr key={idx}>
                  <Td>{i.id}</Td>
                  <Td>{i.title}</Td>
                  <Td>
                    <Button colorScheme="red" size="xs" onClick={(e) => deleteHandler(e, i.id)}>
                      delete
                    </Button>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </>
      )}

    </>
  )
}

const AddRole = () => {

  const [title, setTitle] = useState('')
  const [loading, setLoading] = useState(false)
  const [svrMsg, setSvrMsg] = useState('')

  const submitHandler = async (e) => {
    e.preventDefault()
    try {
      setLoading(true)
      let res = await fetch("http://localhost:3100/api/roles", { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ title: title }) })
    } catch (err) {
      setSvrMsg('something went wrong...')
    } finally {
      setLoading(false)
    }
  }

  return (
    <>
      <Text fontSize="5xl" fontWeight="bold" mb="5">Add Role</Text>
      {svrMsg && <Text color="red">{svrMsg}</Text>}
      <Flex justifyContent="space-between" alignItems="center">
        <FormControl mx={1}>
          <FormLabel>
            Role title
          </FormLabel>
          <Input
            type="text"
            placeholder={"Please enter the title of the role"}
            maxW={500}
            value={title}
            onChange={e => setTitle(e.target.value)}
          />
          <FormHelperText>
            This role can be assigned to users
          </FormHelperText>
        </FormControl>
      </Flex>
      <Button
        my="5"
        type="submit"
        onClick={submitHandler}
        background="#fb5b1f"
        color="white"
        _focus={{ shadow: "" }}
        fontWeight="md"
        disabled={loading}
      >
        {loading ? (
          <>
            loading...
          </>
        ) : (
          <>
            Add Role
          </>
        )}
      </Button>
    </>
  )
}