import React from 'react'
import {
  Tabs,
  TabList,
  TabPanels,
  TabPanel,
  Tab,
  Box,
  Flex,
} from "@chakra-ui/react";


export const SettingsBody = ({ data }) => {
  return (
    <Box>
      <Flex justifyContent="space-between" alignItems="center">
        <Tabs orientation="vertical" w={"100%"}>
          <Box width={"20%"} h="100%" p={2} mx={1} minH={"300px"}>
            <TabList>
              {data.map(i => (
                <Tab
                  key={i.label}
                  _selected={{
                    color: "white",
                    bg: "#fb5b1f"
                  }}
                  _focus={{
                    outline: "none"
                  }}
                >
                  {i.label}
                </Tab>
              ))}
            </TabList>
          </Box>
          <Box width={"80%"} h="100%" border="1px solid rgba(0,0,0,0.3)" borderRadius={10} p={2} mx={1}>
            <TabPanels>
              {data.map(i => (
                <TabPanel key={i.label}>
                  {i.component}
                </TabPanel>
              ))}
            </TabPanels>
          </Box>
        </Tabs>

      </Flex>
    </Box>
  )
}
