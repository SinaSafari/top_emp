import React from 'react'
import { SettingsBody } from '../settingsBody'
import { Box } from '@chakra-ui/react'
import TemplateList from './TemplateList';
import EmailManagement from './EmailManagement';

const EmailSettings = () => {
  const body = [
    {
      id: 1,
      label: "templates",
      component: <TemplateList />
    },
    {
      id: 2,
      label: "managements",
      component: <EmailManagement />
    },
  ];
  return (
    <SettingsBody data={body} />
  )
}

export default EmailSettings
