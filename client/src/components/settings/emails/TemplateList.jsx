import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { Text, Box, Flex, Button } from '@chakra-ui/react'

const TemplateList = () => {
  const [templatesList, setTemplatesList] = useState([])

  const loadData = async () => {
    try {
      let resp = await fetch("http://localhost:3100/api/emailtemplates")
      resp = await resp.json()
      setTemplatesList(resp.data)
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    loadData()
  }, [])

  return (
    <div>
      <Text fontSize={"3xl"} fontWeight={"bold"}>Templates List</Text>
      {templatesList.map(i => (
        <Box key={i.id} border="1px solid rgba(0,0,0,0.3)" borderRadius={10}>
          <Flex p={5} alignItems={"center"} justifyContent={"space-between"} mx={"5"}>
            <Text key={i.id}>{i.title}</Text>
            <Button >detail</Button>
          </Flex>
        </Box>
      ))}
    </div>
  )
}

export default TemplateList
