import React, { useState, useEffect } from 'react'
import { SettingsBody } from '../settingsBody'
import {
  Text,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
  Box,
  Flex,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Spinner,
  IconButton,
} from "@chakra-ui/react";
import { RepeatIcon } from '@chakra-ui/icons'

export const StatusesSetting = () => {
  const body = [
    {
      id: 1,
      label: "Statuses",
      component: <Flex flexDir="column" justifyContent="space-between" >
        <Box mb="5">
          <AddStatus />
        </Box>
        <Box my="5">
          <StatusesList />
        </Box>
      </Flex>
    }
  ]
  return (
    <SettingsBody data={body} />
  )
}


const StatusesList = () => {

  const [statuses, setStatuses] = useState([])
  const [loading, setLoading] = useState(false)
  const [svrMsg, setSvrMsg] = useState('')

  const loadData = async () => {
    try {
      setLoading(true)
      let res = await fetch("http://localhost:3100/api/statuses")
      res = await res.json()
      setStatuses(res.data)
    } catch (err) {
      setSvrMsg("something went wrong")
    } finally {
      setLoading(false)
    }
  }

  const deleteHandler = async (e, id) => {
    try {
      e.preventDefault()
      await fetch(`http://localhost:3100/api/statuses/${id}`, { headers: { "Content-Type": "application/json" }, method: "DELETE" })
    } catch (err) {

    }
  }


  useEffect(() => {
    loadData()
  }, [])



  return (
    <>
      <Flex justifyContent="space-between" alignItems="center">
        <Text fontSize="5xl" fontWeight="bold" mb="5">Statuses List</Text>
        <IconButton
          onClick={loadData}
          aria-label="Refresh"
          background="#fb5b1f"
          color={"white"}
          icon={<RepeatIcon />}
        />
      </Flex>
      {svrMsg && <Text color="red">{svrMsg}</Text>}
      {loading ? (
        <Box>
          <Spinner />
        </Box>
      ) : (
        <>
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>
                  id
                </Th>
                <Th>
                  title
                </Th>
                <Th>
                  actions
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {statuses.map((i, idx) => (
                <Tr key={idx}>
                  <Td>{i.id}</Td>
                  <Td>{i.title}</Td>
                  <Td>
                    <Button colorScheme="red" size="xs" onClick={(e) => deleteHandler(e, i.id)}>
                      delete
                    </Button>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </>
      )}

    </>
  )
}

const AddStatus = () => {

  const [title, setTitle] = useState('')
  const [loading, setLoading] = useState(false)
  const [svrMsg, setSvrMsg] = useState('')

  const submitHandler = async (e) => {
    e.preventDefault()
    try {
      setLoading(true)
      let res = await fetch("http://localhost:3100/api/statuses", { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ title: title }) })
    } catch (err) {
      setSvrMsg('something went wrong...')
    } finally {
      setLoading(false)
    }
  }

  return (
    <>
      <Text fontSize="5xl" fontWeight="bold" mb="5">Add Status</Text>
      {svrMsg && <Text color="red">{svrMsg}</Text>}
      <Flex justifyContent="space-between" alignItems="center">
        <FormControl mx={1}>
          <FormLabel>
            Role title
          </FormLabel>
          <Input
            type="text"
            placeholder={"Please enter the title of the role"}
            maxW={500}
            value={title}
            onChange={e => setTitle(e.target.value)}
          />
          <FormHelperText>
            This role can be assigned to users
          </FormHelperText>
        </FormControl>
      </Flex>
      <Button
        my="5"
        type="submit"
        onClick={submitHandler}
        background="#fb5b1f"
        color="white"
        _focus={{ shadow: "" }}
        fontWeight="md"
        disabled={loading}
      >
        {loading ? (
          <>
            loading...
          </>
        ) : (
          <>
            Add Status
          </>
        )}
      </Button>
    </>
  )
}