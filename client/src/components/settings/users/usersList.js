import {
  Text,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
  Select,
  Tabs,
  TabList,
  TabPanels,
  TabPanel,
  Tab,
  Box,
  Flex,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Divider,
} from "@chakra-ui/react";
import React, { useEffect, useState } from 'react'



export const UsersList = () => {
  const [users, setUsers] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const loadData = async () => {
      try {
        setLoading(true)
        let data = await fetch("http://localhost:3100/api/users")
        data = await data.json()
        console.log('data', data);
        setUsers(data.data)
      } catch (err) {

      } finally {
        setLoading(false)
      }
    }

    loadData()
  }, [])


  return (
    <>
      <Text fontSize="5xl" fontWeight="bold" mb="5">Users List</Text>
      <Table variant="simple">
        <Thead>
          <Tr>
            <Th>
              email
            </Th>
            <Th>
              username
            </Th>
            <Th>
              role
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          {users.map((i, idx) => (
            <Tr key={idx}>
              <Td>{i.email}</Td>
              <Td>{i.username}</Td>
              <Td>{i.role}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </>
  )
}
