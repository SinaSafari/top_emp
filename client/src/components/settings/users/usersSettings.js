import { CreateUserForm } from './createUsersForm'
import { UsersList } from './usersList'
import React from 'react'
import { SettingsBody } from '../settingsBody'


export const UsersSettings = () => {
  const data = [
    {
      id: 1,
      label: "User List",
      component: <UsersList />,
    },
    {
      id: 2,
      label: "Add User",
      component: <CreateUserForm />,
    },
  ]
  return (
    <SettingsBody data={data} />
  )
}