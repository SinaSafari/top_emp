import React, { useEffect, useState } from 'react'

import {
  Text,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
  Select,
  Box,
  Flex,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Divider,
} from "@chakra-ui/react";


export const CreateUserForm = () => {

  const { isOpen, onOpen, onClose } = useDisclosure()

  const [email, setEmail] = useState('')
  const [fullname, setFullname] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [selectedDepartment, setSelectedDepartment] = useState('')
  const [selectedRole, setSelectedRole] = useState('')

  const [deptList, setDeptList] = useState([])
  const [rolesList, setRolesList] = useState([])

  const loadData = async () => {
    try {
      const deptsReq = fetch("http://localhost:3100/api/departments", { headers: { "Content-type": "application/json" } })
      const rolesReq = fetch("http://localhost:3100/api/roles", { headers: { "Content-type": "application/json" } })
      const resp = await Promise.all([deptsReq, rolesReq])
      const deptJson = await resp[0].json()
      const rolesJson = await resp[1].json()
      setDeptList(deptJson.departments)
      setRolesList(rolesJson.data)
    } catch (err) {

    }
  }

  useEffect(() => {
    loadData()
  }, [])

  const onAddUserModal = async () => {
    try {
      if (password !== confirmPassword) {
        // TODO: error handling
      }

      // TODO: recognize role 

      console.log(JSON.stringify({
        email,
        fullname,
        username,
        password,
        selectedDepartment,
        selectedRole,
        role: rolesList.filter(i => i.title == selectedDepartment + "_admin")[0].id, // TODO find a way
      }))

      // TODO: REQUEST:
      let res = await fetch(
        "http://localhost:3100/api/users",
        {
          headers: {
            "Content-Type": "application/json",
          },
          method: "POST",
          body: JSON.stringify({
            email,
            fullname,
            username,
            password,
            selectedDepartment,
            selectedRole,
            role: rolesList.filter(i => i.title == selectedDepartment + "_admin")[0].id, // TODO find a better way
          })
        }
      )
    } catch (err) { }
  }

  return (
    <>
      <Box>
        <Text fontSize="6xl" fontWeight="bold" mb="5">Add User</Text>
        <Box>
          <Flex my={5}>
            <FormControl mx={1}>
              <FormLabel>
                Full name
              </FormLabel>
              <Input
                type="text"
                placeholder={"Please enter the user's fullname"}
                value={fullname}
                onChange={e => setFullname(e.target.value)}
              />
              <FormHelperText>
                The display name will be used in email templates
              </FormHelperText>
            </FormControl>
            <FormControl mx={1}>
              <FormLabel>
                Username
              </FormLabel>
              <Input
                type="text"
                placeholder="Please enter user's username"
                value={username}
                onChange={e => setUsername(e.target.value)}
              />
              <FormHelperText>
                This username could be used for login.
              </FormHelperText>
            </FormControl>
            <FormControl mx={1}>
              <FormLabel>
                Email
              </FormLabel>
              <Input
                type="email"
                placeholder="Please enter the user's email"
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
              <FormHelperText>
                The email will be used in sending events announcement
              </FormHelperText>
            </FormControl>
          </Flex>
          <Flex my={5}>
            <FormControl mx={1}>
              <FormLabel>
                department
              </FormLabel>
              <Select onChange={(e) => setSelectedDepartment(e.target.value)}>
                {/* <option>df</option>
                <option>df</option> */}
                {deptList && deptList.map(i => (
                  <option key={i.id}>{i.title}</option>
                ))}
              </Select>
              <FormHelperText>
                The user should be assigned to a department
              </FormHelperText>
            </FormControl>

            <FormControl mx={1}>
              <FormLabel>
                Role
              </FormLabel>
              <Select onChange={(e) => setSelectedRole(e.target.value)}>
                {/* {rolesList.map((i, idx) => (
                  ["admin", "member"].map((m) => (
                    <option key={idx}>{i.title} - {m}</option>
                  ))
                ))} */}
                {
                  ["admin", "member"].map(i => <option key={i} value={i}>{i}</option>)
                }
              </Select>
              <FormHelperText>
                Role of user in the department
              </FormHelperText>
            </FormControl>
          </Flex>
          <Flex my={5}>
            <Box minW={"50%"}>
              <FormControl mx={1}>
                <FormLabel>
                  Password
                </FormLabel>
                <Input
                  type="password"
                  placeholder={"Please insert a password for user"}
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                />
              </FormControl>
            </Box>
          </Flex>
          <Flex my={5}>
            <Box minW={"50%"}>
              <FormControl mx={1}>
                <FormLabel>
                  Confirm password
                </FormLabel>
                <Input
                  type="password"
                  placeholder={"Confirm the password"}
                  value={confirmPassword}
                  onChange={e => setConfirmPassword(e.target.value)}
                />
              </FormControl>
            </Box>
          </Flex>
          <Flex my={5} alignItems="center" flexDir="row-reverse">
            {/* <Button onClick={onOpen}>
              Add User
            </Button> */}

            <Button
              type="submit"
              onClick={onOpen}
              background="#fb5b1f"
              color="white"
              _focus={{ shadow: "" }}
              fontWeight="md"
            >
              Add User
            </Button>
          </Flex>
        </Box>
      </Box>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            Please confirm the user&apos;s information
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Box my={"3"}>
              <Text fontWeight="bold">
                Username: <span style={{ fontWeight: "normal" }}>{username}</span>
              </Text>
            </Box>
            <Box my={"3"}>
              <Text fontWeight="bold">
                Email: <span style={{ fontWeight: "normal" }}>{email}</span>
              </Text>
            </Box>

            <Box my={"3"}>
              <Text fontWeight="bold">
                Fullname: <span style={{ fontWeight: "normal" }}>{fullname}</span>
              </Text>
            </Box>
            <Divider />
            <Box my={"3"}>
              <Text fontWeight="bold">
                Department: <span style={{ fontWeight: "normal" }}>{selectedDepartment}</span>
              </Text>
            </Box>
            <Box my={"3"}>
              <Text fontWeight="bold">
                Role: <span style={{ fontWeight: "normal" }}>{selectedRole}</span></Text>
            </Box>
            <Divider />
            <Box my={"3"}>
              <Text fontWeight="bold">
                Password: <span style={{ fontWeight: "normal" }}>{password}</span> - confirm: <span style={{ fontWeight: "normal" }}>{confirmPassword}</span>
              </Text>
            </Box>
          </ModalBody>
          <ModalFooter>
            <Flex justifyContent="space-between" alignItems="center" w={"100%"}>
              <Button colorScheme="blue" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button
                type="submit"
                onClick={onAddUserModal}
                background="#fb5b1f"
                color="white"
                _focus={{ shadow: "" }}
                fontWeight="md"
              >
                Add User
              </Button>
            </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}
