const nodemailer = require("nodemailer");


const createTransporter = async () => {

  let testAccount = await nodemailer.createTestAccount();

  let transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST || "smtp.ethereal.email",
    port: process.env.EMAIL_PORT || 587,
    secure: process.env.EMAIL_IS_SECURE || false, // true for 465, false for other ports
    auth: {
      user: process.env.EMAIL_USERNAME || testAccount.user, // generated ethereal user
      pass: process.env.EMAIL_PASSWORD || testAccount.pass, // generated ethereal password
    },
  });

  return transporter
}


const prepareTemplate = async ({ template, context }) => {
  try {
    const templateRaw = await fs.readFile(template, { encoding: "utf-8" })

    return ejs.render(templateRaw, context)
  } catch (err) {
    console.log(err)
  }
}


exports.sendMail = async ({
  template = "next_interview",
  fromEmail = "hr@top.ir",
  subject,
  context,
  recievers,
}) => {

  const transporter = await createTransporter()

  const templatesList = {
    next_interview: "./templates/next_interview.ejs",
    declined: "./templates/decline.ejs"
  }

  const mailContent = await prepareTemplate({ template: templatesList[template], context: context })

  await transporter.sendMail({
    from: fromEmail, // sender address
    to: recievers, // list of receivers
    subject: subject, // Subject line
    // text: "Hello world?", // plain text body
    html: mailContent, // html body
  });
}

