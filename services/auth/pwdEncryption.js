const { compare, genSalt, hash } = require('bcrypt')

exports.verifyPswd = async (rawPswd, pswd) => {
  return await compare(rawPswd, pswd)
}

exports.encryptPswd = async (pswd) => {
  const salt = await genSalt(8)
  return await hash(pswd, salt)
}