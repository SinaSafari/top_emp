const { sign, verify } = require('jsonwebtoken')

exports.singWithPayload = (payload) => {
  return sign(payload, "secret", { expiresIn: "7d" })
}

exports.verifyToken = (token) => {
  return verify(token, "secret")
}