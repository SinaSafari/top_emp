const { sign, verify } = require('jsonwebtoken')

/**
 * 
 * @param {import('express').Request} req 
 */
exports.checkForToken = (req) => {
  let token = ""

  console.log(req.headers.authorization)

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1]
  } else if (
    req.cookies.access_token
  ) {
    token = req.cookies.access_token;
  }

  return token
}