# **_hr.top.ir_**

---

table of contents

- [introduction](#introduction)
- [server](#server)
  - [datamodel](#datamodel)
  - [ORM](#orm)
  - [routes](#routes)
  - [docs](#docs)
- [client](#client)
  - [UI](#ui)
  - [clientside security](#clientside-security)
  - [board](#board)
- [missing features](#missing-features)

---

## **_introduction_**

this application desgined to be customized ATS (Applicant Tracking System) and tries to automate the process of applicant tracking. this process should be automated in different ways like applying, previewing resumes, having talent pool, setting available times for interviewers, registering for applicants in website, setting google calendar events, etc.

---

## **_server_**

### **_datamodel_**

this is the datamodel that is used in current version of the application. its design should change a bit due to storing the history of applicant and their attempts with diffrent resumes.

![](./er.png)

### **_ORM_**

[objection](https://vincit.github.io/objection.js/) is the ORM and [knex](http://knexjs.org/) is the query builder of this application. the structure is fairly simple
and the queries are straight forward.

sqlite is the development database but due to the scale of the app it is appropriate for production as well.

### **_Routes_**

we used express as web framework of this aplication. the architecture of the app follows the principles of clean architecture.

- models and repositories -> entity
- services and controllers -> usecases
- routes -> API

`./public` is static folder that serves by express and all resumes will stored in this location. other static files for further usage could be stored in this directory.

### **_docs_**

the api has ability to use swagger and openapi. you can call `/api-docs` and the openapi is configured. definitions are stored in `./docs/openapi/**/*`

---

## **_client_**

client of this app has been developed using nextjs.

### **_UI_**

[ChakraUI](https://chakra-ui.com/) has been used as ui framework of this app. some of the components are from [chakra-template](https://chakra-templates.dev/) and [choc-ui](https://choc-ui.com/) which provide some pre built component and styles for common usecases. the clientside app does not require any complex ui and animation.

### **_clientside security_**

application uses [next-auth](https://next-auth.js.org/) for client-side security. it uses credential provider for username and password authentication. btw this is not used with a database.

### **_board_**

applicants are shown as kanban board (something like trello). this feature implemented using [@asseinfo/react-kanban](https://github.com/asseinfo/react-kanban). this board and its data prepration methods are used and implemented in `./client/src/pages/applicants/[dept]/[job_offer_id]/index.jsx`. it's not reusable right now, but it could separated like a sharable component with small refactoring.

---

## missing features

there are some features that are missing here is the list of them:

- **applicants history**

one of the requirements was having list of attempts for each applicant. this feature is not implemented yet, and implementing needs massive refactoring specifically in data modeling.

- **integration with google**

another main requirement for this application is to automate application with google api specifically in setting google calendarfor setting events and google meets. this process is not implemented yet because of issues with google authentication and some other same problems. the process should implemented as service in order to be implemented in different proccess or controllers.

- **email and sms template**

editing email and sms templates, also customizing them in different scenarios is the feature that should implemented. also sending email and sms requires job queue and as adapter the server is ready to use [bull](https://github.com/OptimalBits/bull) and [redis](https://redis.io/) as backend. this is fairly simple tool for handling this challenge. but this feature is not tested.

<div style="text-align:center;">

hr.top.ir

</div>
