const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')

/**
 * 
 * @param {import('express').Application} app 
 * @returns {import('express').Application}
 */
const setMiddlewares = (app) => {
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json())
  app.use(cookieParser())
  app.use(cors({ origin: "*" }))
  app.use(helmet())
  app.use("/public", express.static('public'))
  return app
}


/**
 * 
 * @param {import('express').Application} app 
 * @returns {void}
 */
const runApp = (app) => {
  const port = process.env.PORT || 3100
  return app.listen(port, () => console.log(`server is up and running ${port}`))
}


module.exports = {
  setMiddlewares,
  runApp
}