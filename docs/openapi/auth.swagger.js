exports.authSwaggerSchema = {
  login: {
    tags: ["auth"],
    description: "",
    operationId: "authSwaggerSchema",
    responses: {
      "200": {
        description: "successful login",
        "content": {
          "application/json": {
            schema: {
              type: "object",
              properties: {
                token: {
                  type: "string",
                  description: "jwt token",
                },
                user: {
                  type: "object",
                  description: "user object",
                },
              }
            }
          }
        }
      }
    }
  }
}
