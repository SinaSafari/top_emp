const { authSwaggerSchema } = require('./auth.swagger')


const baseSwaggerConfig = {
  servers: [
    {
      url: "http://localhost:3100/api",
      description: "Local server",
    },
    {
      url: "http://localhost:3100/api",
      description: "production server",
    },
  ],
  openapi: "3.0.3",
  info: {
    title: "tom_emp",
    description: "top recruiter panel",
    version: "1.0.0",
  },
  paths: {
    "/login": {
      post: authSwaggerSchema.login
    }
  }
}

module.exports = { baseSwaggerConfig }