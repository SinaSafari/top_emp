const Knex = require('knex')
const { Model } = require('objection')
const { development } = require('../knexfile')

const initialDb = (app) => {
  const knex = Knex(development)
  Model.knex(knex)
  return app
}

module.exports = { initialDb }