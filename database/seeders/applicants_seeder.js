const { Knex } = require("knex");

const statusesList = {
  applied: "applied",
  in_progress: "in_progress",
  accepted_by_hr: "accepted_by_hr",
  accepted_by_admin: "accepted_by_admin",
  first_interview: "first_interview",
  second_interview: "second_interview",
  third_interview: "third_interview",
  final_approval: "final_approval",
  talent_pool: "talent_pool",
  rejected: "rejected",
}

/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return knex("applicants")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("applicants").insert([
        {
          id: 1,
          email: "applicant_tech_front@email.com",
          phone: "989123456781",
          fullname: "Tony Stark",
          resume_path: "/public/resumes/sample.pdf",
          job_offer_id: 1,
          cover_letter: "",
          rate: 5,
          status: statusesList.accepted_by_hr,
          description: "",
          source: "website",
          created_at: Date.now(),
          updated_at: ""
        },
        {
          id: 2,
          email: "SeniorOrganizationalDevelopmentSpecialist@email.com",
          phone: "989123456782",
          fullname: "Peter Parker",
          resume_path: "/public/resumes/sample.pdf",
          job_offer_id: 4,
          cover_letter: "",
          rate: 3,
          status: "",
          description: statusesList.applied,
          source: "website",
          created_at: Date.now(),
          updated_at: ""
        },
        {
          id: 3,
          email: "BackendDeveloper@email.com",
          phone: "989123456783",
          fullname: "Steve Rogers",
          resume_path: "/public/resumes/sample.pdf",
          job_offer_id: 1,
          cover_letter: "",
          rate: 7,
          status: statusesList.applied,
          description: "",
          source: "jobinja",
          created_at: Date.now(),
          updated_at: ""
        },
        {
          id: 4,
          email: "SeniorMarketingSpecialist@email.com",
          phone: "989123456784",
          fullname: "Black Widow",
          resume_path: "/public/sample.pdf",
          job_offer_id: 8,
          cover_letter: "",
          rate: 6,
          status: "final_appreoved",
          description: "",
          source: "irantalent",
          created_at: Date.now(),
          updated_at: ""
        },
        {
          id: 5,
          email: "SeniorMarketingSpecialist1@email.com",
          phone: "989123456785",
          fullname: "Hulk",
          resume_path: "/public/sample.pdf",
          job_offer_id: 8,
          cover_letter: "",
          rate: 8,
          status: "ready_for_fi",
          description: "",
          source: "irantalent",
          created_at: Date.now(),
          updated_at: ""
        },
      ]);
    });
};

module.exports = { seed };