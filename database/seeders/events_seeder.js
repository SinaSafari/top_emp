const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return
  knex("events")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("events").insert([
        {
          id: 1,
          user_id: 2, // tech lead
          applicant_id: 1, // frontend
          other_applicants: "",
          start_event: "2021-05-28T09:00:00+04:30",
          status: "wait_to_held",
          created_at: new Date().toISOString(),
          updated_at: null,
        },
        {
          id: 2,
          user_id: 2, // tech lead
          applicant_id: 3, // backend
          other_applicants: "",
          start_event: "2021-07-28T09:00:00+04:30",
          status: "wait_to_held",
          created_at: new Date().toISOString(),
          updated_at: null,
        },
        {
          id: 3,
          user_id: 5, //marketing lead
          applicant_id: 4, // marketing specialist
          other_applicants: "",
          start_event: "2021-03-28T09:00:00+04:30",
          status: "held",
          created_at: new Date().toISOString(),
          updated_at: null,
        },
        {
          id: 4,
          user_id: 4, // product lead
          applicant_id: 2, // senior organizational development specialist
          other_applicants: "",
          start_event: "2021-12-28T09:00:00+04:30",
          status: "cancled",
          created_at: new Date().toISOString(),
          updated_at: null,
        },
      ]);
    });
};

module.exports = { seed };