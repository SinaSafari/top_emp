const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return;
  // return knex("department_user")
  //   .del()
  //   .then(function () {
  //     // Inserts seed entries
  //     return knex("department_user").insert([
  //       // superadmin
  //       {
  //         id: 1,
  //         dept_id: 1,
  //         user_id: 1,
  //         is_admin: true,
  //       },
  //       {
  //         id: 2,
  //         dept_id: 1,
  //         user_id: 1,
  //         is_admin: true,
  //       },
  //       {
  //         id: 3,
  //         dept_id: 2,
  //         user_id: 1,
  //         is_admin: true,
  //       },
  //       {
  //         id: 4,
  //         dept_id: 2,
  //         user_id: 1,
  //         is_admin: true,
  //       },
  //       // tech
  //       {
  //         id: 5,
  //         dept_id: 1,
  //         user_id: 2, // tech_admin
  //         is_admin: true,
  //       },
  //       // hr
  //       {
  //         id: 6,
  //         dept_id: 2,
  //         user_id: 3, // hr_admin
  //         is_admin: true,
  //       },
  //       // product
  //       {
  //         id: 7,
  //         dept_id: 3,
  //         user_id: 4, // product_admin
  //         is_admin: true,
  //       },
  //       // marketing
  //       {
  //         id: 8,
  //         dept_id: 4,
  //         user_id: 5, // marketing_admin
  //         is_admin: true,
  //       },
  //     ]);
  //   });
};

module.exports = { seed };