const { Knex } = require("knex");

const FrontendDeveloperContent = `
Job description
About Snapp Fintech

At Snapp Fintech, we are going to disrupt the financial industry in Iran and rebuild the concept of financial services for the digital world customers. We have the best talents in tech, product, and business areas and have been developing top-notch products.



Responsibilities

Develop reusable, highly optimized, and testable modules,
Develop and maintain web applications,
Proactively collaborate with team members.,
Ensuring the sharing of knowledge and efﬁcient division of work,
Get exposure to the latest technologies and developments in technology and product-driven company,
Requirements,
4+ years of experience,
Deep Knowledge of JavaScript,
Deep knowledge of React,
Deep knowledge of PWA and service workers,
Familiar with CI/CD,
Strong problem solver,
Strong communication skills,
Familiar with web applications monitoring (Performance, Load Time, etc),
Familiar with Git,

`

const FrontendDeveloperContentJson = {
  aboutRole: "At Snapp Fintech, we are going to disrupt the financial industry in Iran and rebuild the concept of financial services for the digital world customers. We have the best talents in tech, product, and business areas and have been developing top-notch products.",
  responsibilities: `
  
  <ul>
  <li>Develop reusable, highly optimized, and testable modules,</li>
  <li>Develop and maintain web applications,</li>
  <li>Proactively collaborate with team members.,</li>
  <li>Ensuring the sharing of knowledge and efﬁcient division of work,</li>
  <li>Get exposure to the latest technologies and developments in technology and product-driven company,</li>
  </ul>
  



`,
  requirements: `
  Requirements,
  <ul>
  <li>4+ years of experience,</li>
  <li>Deep Knowledge of JavaScript,</li>
  <li>Deep knowledge of React,</li>
  <li>Deep knowledge of PWA and service workers,</li>
  <li>Familiar with CI/CD,</li>
  <li>Strong problem solver,</li>
  <li>Strong communication skills,</li>
  <li>Familiar with web applications monitoring (Performance, Load Time, etc),</li>
  <li>Familiar with Git,</li>
  
  </ul>

  
  `,
}

const BackendDeveloperContent = `
Job description
As a member of our Software Engineering Team, we look first and foremost for people who are passionate about solving business problems through innovation and engineering practices. The ideal candidate is a self-starter with a strong desire to get the jobs done.

It’s a mission that takes some serious smarts, intense curiosity, and determination to be the best. Come be part of the team that makes it possible.





Responsibilities:

Deliver technical solutions based on the business requirement by getting involved in the full development life cycle.
Translate technical requirements into programmed application modules and develop web applications or enhance existing ones.
Must be a collaborative team player with good communication skills.
Participate in code reviews and ensure that all solutions are aligned to predefined architectural specifications.
Work independently or partner with teams.

Requirements
Required Skills:
2+ years of hands-on experience in web development (Java, Golang, C#, PHP, Python, NodeJS, ...)
Deep knowledge about basic programming concepts such as object-oriented programming, software design pattern, modular design, clean code, and data structures.

Design, develop, document, and implement new functionality, as well as build enhancements, modifications, and corrections to existing software.

Relational and non-relational databases

Some Experience in solution design, code reviews, refactoring, and other best practices

Develops unit tests and uses version control (Git)

Understands the Agile mindset and iterative development process

The ability to learn and grasps concepts quickly in a fast-paced environment is critical to success

Willingness to learn to new technologies and frameworks

Preferred Skills:
Experience with Continuous Integration / Continuous Deployment (CI/CD), Test Driven Development (TDD)

Experience with Jenkins, Docker, and Kubernetes.

Knowledgeable of common Java ecosystem technologies such as Spring(and its ecosystem), Reactor, etc is preferred

Experience with message queuing (e.g. RabbitMQ, NATS) and data streaming (e.g. KAFKA)

Experience with SQL and NO-SQL databases including MySql/MariaDB, Redis, MongoDB

Have an understanding of monitoring tools (like Grafana, Elastic APM, Kibana, PMM, etc.)
`
const DatabaseAdministrationContent = `
Job description
About Snapp

Snapp is the pioneer provider of ride-hailing mobile solutions in Iran that connects smartphone owners in need of a ride to Snapp drivers who use their private cars, offering transportation services. We are ambitious, passionate, engaged, and excited about pushing the boundaries of the transportation industry to new frontiers and be the first choice of each user in Iran.



About the Team

We are responsible for maintaining databases within Snapp Box! We are often get involved when all the design decisions have been made and have simply to keep things up and running with the best performance at any time.



About the Role

We are looking for a Database Administrator focusing on MariaDB or MySQL to join Snapp Box! We need high-level skills here in order to run engineering and dealing with other teams that have even a bit of touch with our data. You need to be responsible for the 2nd and 3rd levels of support and also automation within the team.



Responsibilities
You’ll be responsible for

Installation, configuration, and upgrading of SQL servers (Main: MariaDB)
Performs database configuration, monitoring, and tuning
Implement and maintain database security
Backup & Recovery
Security
Replication (Master/Slave & Multi-master)
Partitioning
MariaDB Server Administration
ProxySQL Administration
MariaDB performance tuning and monitoring tools
Documentation
Requirements
Linux administration skills
knowledge of TCP/IP
Strong troubleshooting skills
Being able to plan technical solutions
Well-experience in database clusters
Strong Knowledge of SQL performance tuning
Experience with SQL Monitoring tools
Assist developers with query tuning and schema refinement
Knowledge of clouds
Familiar with open-source SQL servers
Eager to work with open source databases
Good interpersonal communication and presentation skills
Ability to be a team player
`


const SeniorOrganizationalDevelopmentSpecialistContent = `
Job description
About Snapp

Snapp is the pioneer provider of ride-hailing mobile solutions in Iran that connects smartphone owners in need of a ride to drivers who use their private cars offering transportation services. We are ambitious, passionate, engaged, and excited about pushing the boundaries of the transportation industry to new frontiers and be the first choice of each user in Iran.


The main focused areas in our Organizational Development team are as follows:

Leadership Development
People Experience
Performance and Employee Career Development
Organizational Design and Analysis
Digital Organization
People Analytics
Organizational Culture and Values
Change Management
Succession Planning
Voice of Employee and Wellbeing Surveys
Considering your qualifications, you would be involved in some of these areas.

Requirements
MA in HR, Industrial Psychology, Industrial Engineering, Organizational Behavior or MBA with specialization in HR
Proven experience with some of the areas mentioned above
Hands-on experience with Performance Management is a plus
Very good command of English
Communication and Interpersonal Skills
`
const TalentAcquisitionSpecialistContent = `
Job description
About Snapp

Snapp is the pioneer provider of ride-hailing mobile solutions in Iran that connects smartphone owners in need of a ride to drivers who use their private cars offering transportation services. We are ambitious, passionate, engaged, and excited about pushing the boundaries of the transportation industry to new frontiers and be the first choice of each user in Iran.



About the Team

The talent acquisition team is responsible for finding, acquiring, assessing, and hiring prospective candidates to fill roles that are required to meet company hiring goals. Employer branding, future resource planning, diversifying a company’s labor force, and developing a robust candidate pipeline are the cornerstones of talent acquisition.



Responsibilities

Build and maintain a robust pipeline of great candidates
Partner with Hiring Managers to draft, refine and adapt job descriptions by identifying core competencies required for key roles
Evaluates applicants by discussing job requirements and applicant qualifications with managers; interviewing applicants on a consistent set of qualifications
Identifies difficult job vacancies and investigates the best recruitment approach for them
Analyzes the recruitment performance, cost per hire, time to hire and recommends changes and improvements
Keeps the documentation of the recruitment process up to date
Strong command of data and analytics; uses data to influence change
Dedicated focus on developing the team and peers
Actively identify and contribute to Talent Acquisition practices and processes
Lead employer-branding initiatives
Organize and attend job fairs and recruitment events
Requirements
Relevant Bachelor or higher degree
3+ years of recruiting experience
Full-cycle Technical Recruitment experience in a relevant industry (Start-up, Software / Engineering-led businesses)
Experience using and adapting multiple sourcing strategies to meet the needs of complex, technical recruitment activities
Knowledge of recruiting best practices
Very goal-oriented
Excellent communication skills
Good English skills 
Strong networking abilities
Ability to multitask and prioritize to meet definite deadlines
Technical expertise with an ability to understand and explain job requirements for IT roles can be an advantage
It is better for you to be familiar with Applicant Tracking Systems and resume databases
`

const ProductManagerContent = `
Job description
About Snapp

Snapp is the pioneer provider of ride-hailing mobile solutions in Iran. It connects smartphone owners who need a ride to Snapp drivers who use their private cars, offering transportation services. We are ambitious, passionate, engaged, and excited about pushing the boundaries of the transportation industry to new frontiers and be the first choice of each user in Iran.



About Team and Role

On the surface, Snapp’s ride-hailing technology may seem simple: a user requests a ride from the app, and a driver arrives to take their destination. Behind the scenes, however, a giant infrastructure consisting of hundreds of services and terabytes of data supports each and every
trip on the platform.

As a Product Manager, you will be accountable for defining product strategy and collaborating with engineering, marketing, and commercial to bring extraordinary solutions from conception to your customer. You will aggregate customer requirements, work closely with important stakeholders, prioritize new capabilities, build product roadmaps, manage the product life cycle, and bring those capabilities to market. We would like to see someone who has a background in starting a loyalty program for a company from scratch or has come into a current loyalty program and really turned it around with a fresh idea.

Today we are proud to announce that Snapp is the first and biggest ride-hailing service in Iran, with more than 30 million passengers and 2 million drivers in its fleet. We are always expanding the team to reach our ambitious objectives! So if you want to be a part of the journey in revolutionizing the transportation industry, send out your resume.




Responsibilities

Manage the entire feature lifecycle, starting with planning and OKRs, all the way to managing backlog and agile iterations.
Identify, conceptualize and develop new products as well as improvements to existing products to drive efficiency gains and reach profitability.
Communicate effectively with key stakeholders on product features and act as the main contact point for any inquiries.
Collaborate with the engineers and designers and other colleagues sharing knowledge on product developments and issues to discuss and implement effective solutions for the team.
Perform user lab testing and user interviews to synthesize insights and feedback and disseminate that knowledge into the tech teams, building better products.
Help us with creating solutions and visioning product future
Help to communicate with third-party counterparts and ensuring the SLA is always on-track
Provide more meaningful impacts for technical features
Improve data-driven feature suggestions in roadmaps
Designing end to end solutions for requirements
Ability to communicate with third-party for integration
Experience in the KYC/identity management industry is a plus
Product Validation: Defining MVPs, running design sprints, validating requirements throughout customer feedback and development lifecycle
Product Development: Work closely with engineers, designers, and data scientists to execute your roadmap and deliver solutions to the market
Release Planning: Craft key milestones for the product (alpha, beta, GA)
User Advocacy: Ensuring that the voice of the customer is heard every step of the way (from problem validation to solution validation and release)
Requirements
At least three years of prior experience working in a PM role
Proven track record of managing all aspects of a successful product throughout its lifecycle
Proven ability to develop product and marketing strategies and effectively communicate recommendations to executive management
Strong analytical and quantitative skills with the ability to use data and metrics to justify requirements, features and drive the management of product
Solid technical background with understanding and/or hands-on experience in software development and web technologies
MS/BS degree in Computer Science, Engineering, Business or equivalent preferred
An active interest in commerce platforms. Strong background or familiarity with eCommerce development, Web Services, and different loyalty system programs.
Complete Understanding of software product life cycles for multi-stage long-term strategic initiatives for enterprise software or web-based products.
A working understanding of emerging technology areas like mobile applications and social networks.
Ability to work with cross-functional teams including sales, marketing, business management, or customer care.
Knowledge of design thinking methodologies to create outstanding user experiences with the help of UX or product designers.
Proven experience in developing and maintaining good relationships with key stakeholders, partners, and executive management.
Strong business analysis skills, techniques including writing user stories, acceptance criteria, and documenting requirements.
Knowledge of Feature impact analysis
Excellent prioritization skills and ROI mindset
Ability to design meaningful and measurable metrics
Stakeholder management skills
`
const UXResearchLeadContent = `
Job description
About Snapp

Snapp is the pioneer provider of ride-hailing mobile solutions in Iran that connects smartphone owners in need of a ride to Snapp drivers who use their private cars offering transportation services. We are ambitious, passionate, engaged, and excited about pushing the boundaries of the transportation industry to new frontiers and be the first choice of each user in Iran.



About The Role

As a UX Research lead at Snapp, the candidate will be involved in the product development life cycle across the company and will collaborate with designers, UX writers, product managers, and engineers to identify research roadmap activities to support product feature releases and future concepts for exploration.

He/she will plan, supervise and lead all aspects of research including, project scoping, scheduling, staffing, recruiting, legal logistics, technical logistics, study moderation, data analysis, reporting, and team communication. Depending on the scope and complexity of research, the candidate may plan and execute studies as a sole researcher or may lead and manage the UXR team to perform the work.

The candidate will Initiate and oversee the execution of each research effort and will consult on human factors, UCD best practices, and recommend research activities. Also, he/she will educate stakeholders on the importance of UX research and on the other hand will educate team members and the field on the use of new tools. The candidate will manage, mentor, and support junior to senior UX Researchers.



Responsibilities

Manage a research team and handle complexity

Set clear expectations and goals for individuals and the team.

Make staffing decisions based on business priorities, both short-term and long-term, as well as team strengths and motivation.

Adjust management style to effectively get the most out of each team member.

Engaging the stakeholders in research practice in all stages of research

Shape team strategy, identifying risks, and prioritizing opportunities.

Identify gaps and solutions for future growth and scale within the team or organization, including system, product, function, partnership, culture, and/or organization opportunities.

Gather data, synthesize it into actionable insights, and share it in compelling ways

Partner with third parties to define, create and execute remote studies as well

Requirements
+4 years of experience in user experience research

Significant hands-on experience planning and conducting a variety of user research methods (generative, evaluative, qualitative, quantitative) in a product development context

Experience building and setting research strategy that connects with business strategy

Have a point of view about strategic decisions, using insights from the team. Understand how to influence stakeholders and leaders to implement the best ideas.

Able to maintain methodical rigor, while adjusting to changing timelines or priorities

Experience mentoring entry-level researchers with an eagerness to grow people management skills

Superior communication skills and storytelling ability

Become a trusted, strategic partner to product stakeholders by communicating and demonstrating the value of user research, and by delivering insights that lead to demonstrable business value.

Demonstrates humility, honesty, curiosity, and resourcefulness
`

const SeniorMarketingSpecialistContent = `
Job description
About Snapp

Snapp is the pioneer provider of ride-hailing mobile solutions in Iran that connects smartphone owners in need of a ride to drivers who use their private cars offering transportation services. We are ambitious, passionate, engaged, and excited about pushing the boundaries of the transportation industry to new frontiers and be the first choice of each user in Iran.


Responsibilities

Will be Involved in Driver Image and Branding activities (both online and offline).
Make sure to have consistent and effective message in driver marketing campaigns.
Responsible for campaign execution and side activities.
Internal and cross department coordination
Be able to Harmonize the team and supervise the team members
Requirements
At least 4 years of experience in either of the following fields: Brand Manager, Brand Strategist or Marketing Manager
Hands-on experience in running marketing campaign
Team player & experienced in team management.
Data driven person
Intermediate (or advance) level of Excel software
Intermediate level of English language
Strong communication (verbal and written) & presentation skills
Organized and punctual.
`



/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return knex("job_offers")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("job_offers").insert([
        {
          id: 1,
          dept_id: 1,
          title: "Frontend Developer",
          content: JSON.stringify(FrontendDeveloperContentJson),
          status: "draft",
          created_at: Date.now(),
          updated_at: null
        },
        // {
        //   id: 2,
        //   dept_id: 1,
        //   title: "Senior Backend Developer",
        //   content: BackendDeveloperContent,
        //   created_at: Date.now(),
        //   updated_at: null
        // },
        // {
        //   id: 3,
        //   dept_id: 1,
        //   title: "Database Administrator",
        //   content: DatabaseAdministrationContent,
        //   created_at: Date.now(),
        //   updated_at: null
        // },
        // {
        //   id: 4,
        //   dept_id: 2,
        //   title: "Senior Organizational Development Specialist",
        //   content: SeniorOrganizationalDevelopmentSpecialistContent,
        //   created_at: Date.now(),
        //   updated_at: null
        // },
        // {
        //   id: 5,
        //   dept_id: 2,
        //   title: "Talent Acquisition Specialist",
        //   content: TalentAcquisitionSpecialistContent,
        //   created_at: Date.now(),
        //   updated_at: null
        // },
        // {
        //   id: 6,
        //   dept_id: 3,
        //   title: "Product Manager",
        //   content: ProductManagerContent,
        //   created_at: Date.now(),
        //   updated_at: null
        // },
        // {
        //   id: 7,
        //   dept_id: 3,
        //   title: "UX Research Lead",
        //   content: UXResearchLeadContent,
        //   created_at: Date.now(),
        //   updated_at: null
        // },
        // {
        //   id: 8,
        //   dept_id: 4,
        //   title: "Senior Marketing Specialist",
        //   content: SeniorMarketingSpecialistContent,
        //   created_at: Date.now(),
        //   updated_at: null
        // },
      ]);
    });
};

module.exports = { seed };