const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return knex("roles")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("roles").insert([
        { id: 1, title: "superadmin" },
        { id: 2, title: "tech_admin" },
        { id: 3, title: "product_admin" },
        { id: 4, title: "hr_admin" },
        { id: 5, title: "finance_admin" },
        { id: 6, title: "merketing_admin" },
      ]);
    });
};

module.exports = { seed };