const { Knex } = require("knex");
const { generateDateTime } = require('../../lib/utils/time')

/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return knex("available_times")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("available_times").insert([
        {
          id: 1,
          user_id: 2, // tech
          applicant_id: 1, // backend
          dept_id: 1,
          start_time: generateDateTime({ day: 18, hour: 12 }),
          end_time: generateDateTime({ day: 18, hour: 14 }),
        },
        {
          id: 2,
          user_id: 2, // tech
          applicant_id: 1, // backend
          dept_id: 1,
          start_time: generateDateTime({ day: 18, hour: 9 }),
          end_time: generateDateTime({ day: 18, hour: 11 }),
        },
        {
          id: 3,
          user_id: 4, // product
          applicant_id: null, // not set
          dept_id: 3,
          start_time: generateDateTime({ day: 21, hour: 14 }),
          end_time: generateDateTime({ day: 21, hour: 16 }),
        },
      ]);
    });
};

module.exports = { seed };