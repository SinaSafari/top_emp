
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('departments').del()
    .then(function () {
      // Inserts seed entries
      return knex('departments').insert([
        { id: 1, title: 'tech' },
        { id: 2, title: 'hr' },
        { id: 3, title: 'product' },
        { id: 4, title: "marketing" },
        { id: 5, title: "finanace" },
      ]);
    });
};
