const { Knex } = require("knex");


const statusesList = {
  applied: "applied",
  in_progress: "in_progress",
  accepted_by_hr: "accepted_by_hr",
  accepted_by_admin: "accepted_by_admin",
  first_interview: "first_interview",
  second_interview: "second_interview",
  third_interview: "third_interview",
  final_approval: "final_approval",
  talent_pool: "talent_pool",
  rejected: "rejected",
}

/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return knex("statuses")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("statuses").insert([
        {
          id: 1,
          title: "applied"
        },
        {
          id: 2,
          title: "in_progress"
        },
        {
          id: 3,
          title: "accepted_by_hr"
        },
        {
          id: 4,
          title: "accepted_by_admin"
        },
        {
          id: 5,
          title: "first_interview"
        },
        {
          id: 6,
          title: "second_interview"
        },
        {
          id: 7,
          title: "third_interview"
        },
        {
          id: 8,
          title: "final_approval"
        },
        {
          id: 9,
          title: "talent_pool"
        },
        {
          id: 10,
          title: "rejected"
        },
      ]);
    });
};

module.exports = { seed };