const { Knex } = require("knex");
const bcrypt = require('bcrypt')


/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return knex("users")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("users").insert([
        {
          id: 1,
          email: "superadmin@email.com",
          username: "superadmin",
          password: "$2b$08$Ikp7nyladjOAnWdXJx1eze3xCmK6Hyh3YJBGhfNrO5vuN7zGegAry", // password
          // role: "superadmin",
          role_type: "admin",
          role_id: 1,
          created_at: Date.now(),
          updated_at: null,
        },
        // {
        //   id: 2,
        //   email: "tech@email.com",
        //   username: "tech admin",
        //   password: "$2b$08$Ikp7nyladjOAnWdXJx1eze3xCmK6Hyh3YJBGhfNrO5vuN7zGegAry", // password
        //   role: "tech_admin",
        //   created_at: Date.now(),
        //   updated_at: null,
        // },
        // {
        //   id: 3,
        //   email: "hr@email.com",
        //   username: "HR admin",
        //   password: "$2b$08$Ikp7nyladjOAnWdXJx1eze3xCmK6Hyh3YJBGhfNrO5vuN7zGegAry", // password
        //   role: "hr_admin",
        //   created_at: Date.now(),
        //   updated_at: null,
        // },
        // {
        //   id: 4,
        //   email: "product@email.com",
        //   username: "product admin",
        //   password: "$2b$08$Ikp7nyladjOAnWdXJx1eze3xCmK6Hyh3YJBGhfNrO5vuN7zGegAry", // password
        //   role: "product_admin",
        //   created_at: Date.now(),
        //   updated_at: null,
        // },
        // {
        //   id: 5,
        //   email: "marketing@email.com",
        //   username: "marketing admin",
        //   password: "$2b$08$Ikp7nyladjOAnWdXJx1eze3xCmK6Hyh3YJBGhfNrO5vuN7zGegAry", // password
        //   role: "marketing_admin",
        //   created_at: Date.now(),
        //   updated_at: null,
        // },
      ]);
    });
};

module.exports = { seed };