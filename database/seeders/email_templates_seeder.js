const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const seed = (knex) => {
  return knex("email_templates")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("email_templates").insert([
        {
          id: 1,
          template: "<html><head><title>hello world</title></head><body><h4>Hello world</h4></body></html>",
          title: "reject announcement email template",
          required_data: "",
          created_at: Date.now(),
          updated_at: Date.now()
        },


      ]);
    });
};

module.exports = { seed };