const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema.createTable("job_offers", (t) => {
    t.increments('id').primary()
    t.integer('dept_id').references('id').inTable('departments')
    t.string('status')
    t.string('title')
    t.text('content')
    t.timestamps()
  });
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("job_offers");
};

module.exports = { up, down };