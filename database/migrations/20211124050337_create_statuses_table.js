const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema.createTable("statuses", (t) => {
    t.increments('id')
    t.string('title')
  });
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("statuses");
};

module.exports = { up, down };