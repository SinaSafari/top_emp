const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema
    .createTable("roles", (t) => {
      t.increments("id")
      t.string("title")
    })
    .alterTable("users", (t) => {
      t.integer("role_id").references("id").inTable("roles")
    })
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("roles");
};

module.exports = { up, down };