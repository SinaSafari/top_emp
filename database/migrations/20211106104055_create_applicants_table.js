const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema.createTable("applicants", (t) => {
    t.increments("id").primary()
    t.string("email")
    t.string("phone")
    t.string("fullname")
    t.string("resume_path")
    t.integer('job_offer_id').references('id').inTable('job_offers')
    t.string("cover_letter")
    t.tinyint("rate")
    // add statuses
    t.string("status").comment("status of the applicant which can be 'first_interview', 'accepted', etc.")
    t.string("description").comment("description that added by the admin if the resume is imported from other source")
    t.string("source").comment("it describes the source of resume, like 'site', 'jobinja', etc.")
    t.timestamps()
  });
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("applicants");
};

module.exports = { up, down };