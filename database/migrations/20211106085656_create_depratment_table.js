const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema
    .createTable("departments", (t) => {
      t.increments('id')
      t.string('title')
    })
    .alterTable("users", (t) => {
      t.integer("dept_id").references("id").inTable("departments")
    })

  // .createTable('department_user', (t) => {
  //   t.increments('id')
  //   t.integer('dept_id').references('id').inTable('departments')
  //   t.integer('user_id').references('id').inTable('users')
  //   t.boolean('is_admin').defaultTo(false)
  // })
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable('departments').dropTable('department_user')
};

module.exports = { up, down };