const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema.createTable("events", (t) => {
    t.increments('id').primary()
    t.integer('user_id').references('id').inTable('users')
    t.integer('applicant_id').references('id').inTable('applicants')
    t.string('other_participants').comment('comma separated list of emails which are invited to the event')
    t.tinyint("rate")
    t.string("start_time").comment('for format: https://stackoverflow.com/a/52949790')
    t.string("end_time").comment('for format: https://stackoverflow.com/a/52949790')
    t.string('type').defaultTo('wait_to_held').comment("it can be 'wait_to_held', 'held', and 'cancled'")
    t.string("description")
    t.timestamps()
  });
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("events");
};

module.exports = { up, down };