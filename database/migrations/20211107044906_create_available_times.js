const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema.createTable("available_times", (t) => {
    t.increments('id')
    t.integer('user_id').references('id').inTable('users')
    t.integer('dept_id').references('id').inTable('users') // free times of the departments
    t.integer('applicant_id').references('id').inTable('applicants').nullable()
    t.dateTime('start_time')
    t.dateTime('end_time')
  });
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("available_times");
};

module.exports = { up, down };