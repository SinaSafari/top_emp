const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema.createTable("email_templates", (t) => {
    t.increments('id').primary()
    t.string('title')
    t.string('required_data')
    t.text("template")
    t.timestamps()
  });
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("email_templates");
};

module.exports = { up, down };