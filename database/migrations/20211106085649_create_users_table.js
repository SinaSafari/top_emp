const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
const up = (knex) => {
  return knex.schema.createTable("users", (t) => {
    // fullname
    // email
    // username
    // password
    // deaprtment
    // role_type - member or admin
    // role_id - references on roles table

    t.increments('id').primary()
    t.string('email')
    t.string('username')
    t.string('fullname')
    t.string('password')
    t.string('role_type')
    t.timestamps()
  });
};

/**
 *
 * @param {Knex} knex
 */
const down = (knex) => {
  return knex.schema.dropTable("users");
};

module.exports = { up, down };